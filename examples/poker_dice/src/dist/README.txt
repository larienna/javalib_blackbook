P O K E R   D I C E
July 2019

Powered by
[|] Black Book

Programmed by 
Eric Pietrocupo

Game design as an example to use the black book library. Source code available on gitlab in the:

[Black Book library project] (https://gitlab.com/larienna/javalib_blackbook)

## Usage

To run the program, simply go in the "bin" folder and execute the .bat script if you are in windows, or the other script if you are in Linux / Mac OS. The the program is made in java, it should run on any desktop where java is installed.

## Rules of the game

You roll five 6 sided and try to make poker hands with the faces you roll. Faces are identified as:

9: NIne
T: Ten
J: Jack
Q: Queen
K: King
A: Ace

You can keep from 0 to 5 dice once and reroll the rest of the dices. The resulting hand is your final hand.

The quality of your resulting hand will multiply your bid and give you the result as winnings.

## Commands

You need to start the game with 

start

You can show various information with 

show player
show payout

You can change your bid to 50$ with

bid 50

you can make your roll by typing

roll

This is going to put you in the REROLL context where bid cannot be chanced and only dice can be kept with the command

Keep 1 3 5

To keep the die 1 3 and 5. One that is done, the game automatically reroll unkept dices and pay your resulting hand.




Enjoy and have fun!


