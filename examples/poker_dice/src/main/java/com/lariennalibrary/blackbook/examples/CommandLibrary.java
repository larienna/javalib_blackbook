/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.examples;
import com.lariennalibrary.blackbook.BB;
import com.lariennalibrary.blackbook.ICommand;
import java.util.ArrayList;

/**
 * List of commands for this game defines as a list of static class
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public final class CommandLibrary 
{
   
   public static class ShowPlayer implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.printf ("Money: %d\n", RuleBox.data.money);
         BB.out.printf ("Current Bid: %d\n", RuleBox.data.bid);
         BB.out.printf ("Reroll left: %d\n", RuleBox.data.reroll_left);
         BB.out.printf ("Dices: %s\n", RuleBox.data.get_printed_rolls());
         
         BB.out.println();
         return 0;
      }

      @Override
      public String help()
      {  return ("Display information about the player");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
   
   public static class ShowPayout implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("--- Payout of the various hands ---");
         for ( EPayout payout: EPayout.values())
         {
            BB.out.printf ("%-15s bid x %7.2f\n", payout.name, payout.multiplier );
         }
         
         return 0;
      }

      @Override
      public String help()
      {  return ("Display the payout multipliers of the various hands");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
   
   public static class StartGame implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("Starting the game");
         BB.set_context("NEWROLL");
         return 0;
      }

      @Override
      public String help()
      {  return ("Starts the game with the currently configured parameters");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }       
    }
   
   public static class ChangeBid implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  //Assume there is only 1 parameter and that it is an integer
         int newvalue = Integer.valueOf(param.iterator().next());
         
         if ( newvalue <= RuleBox.data.money ) 
         {
            RuleBox.data.bid = newvalue;
            BB.out.printf ("bid changed to %d\n", newvalue );
         }
         else BB.out.printf ("Cannot set bid to %d since it's greater than the amount of money %d\n"
                 , newvalue, RuleBox.data.money );
         
         return 0;
      }

      @Override
      public String help()
      {  return ("Change the current bid of the player, must be lower than money");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }       
    }
   
   public static class RollDice implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  int tmproll;
         
         for ( int i = 0 ; i < RuleBox.data.dice.length ; i++)
         {  tmproll = (int)((Math.random() * 6.0f) );
            if ( tmproll > 5 ) tmproll = 5;
            RuleBox.data.dice[i] = tmproll;            
         }
         RuleBox.data.sort_dice();
         
         BB.out.printf ("Roll Results: %s\n", RuleBox.data.get_printed_rolls());
         BB.out.printf ("Rerolls left: %d\n", RuleBox.data.reroll_left );
         
         BB.set_context("REROLL");
         
         return 0;
      }

      @Override
      public String help()
      {  return ("Roll all the dice for the first time");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }       
    }
   
   public static class KeepDice implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  int tmproll;
         int tmpvalue;
         EPayout handtype;
         int payout;
         boolean [] hold = new boolean[RuleBox.NB_DICE];
         
         //initialise the hold array
         for ( int i = 0 ; i < hold.length ; i++) hold[i] = false;
         
         //set dice on hold with the number typed
         if ( param.size() > 0)
         {
            for ( String value: param)
            {  
               if ( value.isEmpty() == false)
               {
                  tmpvalue = Integer.valueOf(value);
                  if ( tmpvalue > 0 && tmpvalue <= 5) hold[tmpvalue-1] = true;
                  else BB.out.printf ("ERR: Ignored die %d\n", tmpvalue);
               }
            }
         }         
         
         //reroll unhold dice
         for ( int i = 0 ; i < RuleBox.data.dice.length ; i++)
         {  if ( hold[i] == false)
            {
               tmproll = (int)((Math.random() * 6.0f) );
               if ( tmproll > 5 ) tmproll = 5;
               RuleBox.data.dice[i] = tmproll;            
            }
         }
         RuleBox.data.sort_dice();
         
         RuleBox.data.reroll_left--;
         
         BB.out.printf ("Re-roll Results: %s\n", RuleBox.data.get_printed_rolls());
         
         if ( RuleBox.data.reroll_left > 0 )
         {
            
            BB.out.printf ("Rerolls left: %d\n", RuleBox.data.reroll_left );
         }
         else // roll is over, pay the player and change context
         {
         
            BB.set_context("NEWROLL");
            RuleBox.data.reroll_left = RuleBox.NB_REROLL;
            RuleBox.data.money -= RuleBox.data.bid;
            handtype = EPayout.analyse_hand(RuleBox.data.dice);
            payout = (int)(handtype.multiplier * RuleBox.data.bid);
            RuleBox.data.money += payout;
            BB.out.printf("Your hand is : %s\n", handtype.name);
            BB.out.printf("You won : %d$\n", payout);            
         }
         return 0;
      }

      @Override
      public String help()
      {  return ("Specify the dice to keep with their number(1-5), and reroll the rest (example: keep 3 4 5)");  
      }      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }       
      
      
    }
   
}
