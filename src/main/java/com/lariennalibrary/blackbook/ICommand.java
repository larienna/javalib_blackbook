/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

import java.util.ArrayList;

/**
 * Interface to be used for implementing commands. This is where you set the 
 * code to run when executing a command. There is also getter functions that
 * returns all contant values. Some of them, like arraylists will have to be 
 * build in advance in the constructor of the command.
 * 
 * Those commands should be constructed once by the command list, so they are
 * somewhat singletons. It's the only way that function pointers can be simulated
 * in java.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public interface ICommand 
{
    /** 
    * This will be the lines of code executed when the command is launched
    * 
    * @param param   this is the list of parameters in sequential order
    * @return an error code or 0 if it's a success
    */
   public int execute ( ArrayList<String> param );
   //TODO EXECUTE RETURN not sure if return value is the iteration number to come back to since
   // it could be non sequential. THe information could also be as buffer in
   // BB class, terminal will read and write from and to it.
   //TODO PARAM MANAGEMENT not sure if the values will still be passed as string
   //They will be validated value so it could be ok.
   
   /**
     * A text of string that explain the syntax to use this command. 
     * It could be possible to place the information in the database or generate
     * it with some code like in Picocli.
     * 
     * @return string of text that explains the command
     */
   public String help ();
   
   /**
    * Name of the command to use on the command line.    
    * 
    * @return the name of the command, it's a constant string.
    */
   //public String name();
      
   /**
    * Name of the parameters required by the command line. The list is constant
    * and should be built during object contruction. It's simply a list of 
    * strings. If the list of parameter is empty, simply return null.
    * 
    * @return a list of parameters
    */   
   public ArrayList<String> param_name();
   
   
}
