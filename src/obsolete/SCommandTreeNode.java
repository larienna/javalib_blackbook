/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

import java.util.ArrayList;
import java.util.Iterator;
//import java.util.Map;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Set;

/**
 * Tree node data structure to create hierarichal structure of commands. The
 * Specifications are:
 * 
 * - The system is designed to be read only, you can add stuff and build your 
 *   tree but once built, it can hardly be modified. 
 * - The structure handle empty nodes with childs, or node with commands and no 
 *   child. So only the leafs has commands. 
 * - The structure is also unidirectional, you can read from root to leafs
 * - Multiple nodes can point on the same list of childs.
 * 
 * The implementation has been inspired from the content of this thread:
 * https://stackoverflow.com/questions/19330731/tree-implementation-in-java-root-parents-and-children
 * 
 * design notes: the structure was originally designed as a generic structure 
 * but it prevent the chaining of calls when building up the tree since it lost
 * it's datatype after a first call. I would have required to store the node
 * in a temporary variable to add up new childs.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class SCommandTreeNode 
{  
   private final ICommand _data;
   //private final Map<String, SCommandTreeNode> _child;
   private final ArrayList<SCommandTreeNode> _child;
   private final String _name;
   //private SCommandTreeNode _parent = null;
   
   /**
    * Constructor called if the node is empty, and will only contain children
    * @param name of the node to create
    */
   public SCommandTreeNode ( String name )
   {
      _child = new ArrayList<>();      
      _data = null;
      _name = name;
   }
   
   /**
    * Constructor called when the node will contain a command and no childrens.
    * @param data 
    */
   
   public SCommandTreeNode ( ICommand data )
   {
      _data = data;
      _child = null;
      //BUGFIX THIS IS A QUICK FIX because neme was removed from I Command
      //_name = data.name();
      _name = "Allo";
   }
   
   /**
    * Getter to return the name, allow keeping all fields private for consistency
     @return the name of the node
    */
   public String name () { return _name;}
           
   
   /**
    * Used when adding an empty node that will contain other childrens.
    * @param name of the command
    */
   
   public void add ( String name )
   {
      if ( _child != null)
      {
         _child.add ( new SCommandTreeNode( name ));
      }
      else System.out.println ("BB ERROR: SCommandTreeNode.add(String): _child is null, cannot add node");
   }
   
   /**
    * This is used when adding a leaf node that contains a command
    * 
    //* @param key name to reference the element
    * @param data content of the leaf node
    */
   public void add ( ICommand data )
   {
      if ( _child != null )
      {
         _child.add(new SCommandTreeNode(data));
      }
      else System.out.println ("BB ERROR: SCommandTreeNode.add(ICommand): _child is null, cannot add command");
   }
   
   /**
    * Used when you want to add a node of childrens already created if the command
    * has already been added before. Avoid creating multiple instances of the 
    * same commands
    * 
    * @param childrens A previous node with commands
    */
   public void add (SCommandTreeNode childrens)
   {
      if ( _child != null)
      {
         if ( childrens != null) _child.add (childrens);
         else System.out.println ("BB ERROR: SCommandTreeNode.add(SCommandTreeNode): childrens is null, cannot add node");
      }
      else System.out.println ("BB ERROR: SCommandTreeNode.add(SCommandTreeNode): _child is null, cannot add node");
   }
   
   /**
    * Method that returns a child with a matching name
    * 
    * @param name of the child to return
    * @return a reference on the child node else null if nothing can be found.
    */
   
   public SCommandTreeNode child ( String name )
   {
      if ( _child != null )
      {
         SCommandTreeNode node = null;
         boolean found = false;
         Iterator<SCommandTreeNode> itr = _child.iterator();
         while (itr.hasNext() == true && found == false)
         {  node = itr.next();
            if ( node._name.equals(name)) found = true;
         }
         if ( found == true) return node;
         else return null;
      }         
      else return null;
   }
   
   /**
    * Returns the list of childrens in the node to iterate over them.
    * @return 
    */
   
   public ArrayList<SCommandTreeNode> childrens ()
   {
      return _child;      
   }
   
   /**
    * Method that returns the data in the current node
    * @return a reference on the data
    */
   
   public ICommand command ()
   {
      return _data;
   }
   
   /**
    * Check if the node is a leaf node
    * 
    * @return true of the node is a leaf node and has data.
    */
   
   public boolean is_leaf ()
   {
      return (_data != null && _child == null);      
   }
}
