/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.demo;

import com.lariennalibrary.blackbook.BB;
import com.lariennalibrary.blackbook.ICommand;
//import com.lariennalibrary.blackbook.SCommandTreeNode;
//import com.lariennalibrary.blackbook.SContext;
import com.lariennalibrary.blackbook.IRuleBox;
import com.lariennalibrary.blackbook.demo.BlackBookDemo.CmdAdd;
import com.lariennalibrary.blackbook.demo.BlackBookDemo.CmdBonjour;
import com.lariennalibrary.blackbook.demo.BlackBookDemo.CmdHello;
import com.lariennalibrary.blackbook.terminal.SwingTerminal;
import com.lariennalibrary.blackbook.terminal.SwingTerminalLauncher;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;


/**
 * This is a series of testing methods to be used trought the development
 * and maybe get converted later as real tests
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class DemoTest 
{
   public static void buffers ( IRuleBox rulebox )
   {
      //System.out.println("Youhou! test");
      BB.init(rulebox);
      //System.out.println("Youhou! test");
      
      for ( int i = 0 ; i < 26 ; i++)
      {
         BB.out.println((char)('a' + i) );                                   
      }
      
      /*BB.out.print ("\nThis is a boolean: " );
      BB.out.println (true);
      BB.out.printf ("This string|\nShould be displayed|\n on %d different lines", 3);
      BB.out.print ('\n');
      BB.out.println ( 25 );
      BB.out.println ( 25.250f );
      BB.out.println ( 26.300 );
      BB.out.print ("This should skip 3 lines\n\n\n\n3 lines has been skipped\n");
      
      for ( int i = 0 ; i < 10 ; i++)
      {
         BB.out.print( i );                                   
      }
      BB.out.flush();
      
      BB.out.print ("there seems to be a problem\n");
      BB.out.print ("with beginning and trailing new line characters\n");
      BB.out.print ("the sentense you read should be on multple lines\n");
      BB.out.printf ("I am trying again with prinf instead\n");
      BB.out.printf ("This should be on 2 different lines\n");
      BB.out.println ("This should also appear");
      BB.out.println ("on 2 different lines");*/
      
      System.out.println ( "Buffer: \n" + BB.out);
   }
   
   public static void commands ( IRuleBox rulebox)
   {
      BB.init(rulebox);
      
      ICommand cmd1 = new CmdBonjour();
      ICommand cmd2 = new CmdHello();
      ICommand cmd3 = new CmdAdd();
      
      cmd1.execute( null );
      cmd2.execute( null );
      cmd3.execute( null );
      
      
      
      
      
      //System.out.printf ("\nCommand Name: %s ; Param: %s", cmd1.name(), cmd1.param_name());
      //System.out.printf ("\nCommand Name: %s ; Param: %s", cmd2.name(), cmd2.param_name());
      //System.out.printf ("\nCommand Name: %s ; Param: %s", cmd3.name(), cmd3.param_name());
      
      System.out.println ("\n--- Console output ---");
      System.out.println( BB.out );
   }
   
   public static void command_tree (IRuleBox rulebox)
   {
      BB.init(rulebox);
      
      
      //SCommandTreeNode root = new SCommandTreeNode();
      //STreeNode<String, ICommand> show = new SCommandTreeNode<>(); // parent command
      //STreeNode<String, ICommand> compute = new SCommandTreeNode<>(); // parent command
      //ctx.put ("show", show );
      //ctx.put ("compute", compute );
      
      rulebox.application_startup();
      /*SContext gamectx = new SContext ("GAME");
      BB.context_list.add (gamectx);
            
      gamectx.commands.add ("show");
      gamectx.commands.add ("compute");
      gamectx.commands.add ("subranch");
      
      
      
      gamectx.commands.child("show").add( new CmdBonjour());      
      gamectx.commands.child("show").add( new CmdHello());      
      gamectx.commands.child("compute").add( new CmdAdd());
      gamectx.commands.child("subranch").add (gamectx.commands.child("show") );*/
      
      //this should generate an error
      //root.child("show").child("bonjour").add("bonjour", new CmdBonjour());
     
      //show.add ("bonjour", new CmdBonjour);
      //show.add ("hello", new CmdHello);
      //compute.add ("add", new CmdAdd);
      
      System.out.println ("--- Reading the entire command tree ---");
      
      /*for ( SContext ctx: BB.context_list )
      {
         System.out.println ( ctx.name );
         recursive_command ( ctx.commands, 1);   
      }  */ 
      
      
      
      
   }
   
   //OBSOLETE used in the old tree like command structure
   /*private static void recursive_command ( SCommandTreeNode node, int level)
   {
            
      //if there is additional childs
      if ( node.is_leaf() == false)
      {         
         for ( SCommandTreeNode tmpnode: node.childrens() )
         {
            System.out.println();
            for ( int i = 0 ; i < level; i++) System.out.print ("- ");
            System.out.print (tmpnode.name()); 
            recursive_command ( tmpnode, level + 1);
         }
      }
   }*/
   
   public static void introscreen (IRuleBox rulebox)
   {
      
      BB.init( rulebox);
      
      System.out.println ("\n--- Console output ---");
      System.out.println( BB.out );
   }
   
   public static void interpreter( IRuleBox rulebox)
   {
      BB.init( rulebox);
      
      rulebox.application_startup();
      //building the test command tree
      /*SContext gamectx = new SContext ("GAME");
      BB.context_list.add (gamectx);
      
      gamectx.commands.add ("show");
      gamectx.commands.add ("compute");
      gamectx.commands.add ("subranch");    
            
      gamectx.commands.child("show").add( new CmdBonjour());      
      gamectx.commands.child("show").add( new CmdHello());      
      gamectx.commands.child("compute").add( new CmdAdd());
      gamectx.commands.child("subranch").add (gamectx.commands.child("show") );
      
      BB.active_context = gamectx;*/
      
      System.out.println ("--- Reading the entire command tree ---");
      
      /*for ( SContext ctx: BB.context_list )
      {
         System.out.println ( ctx.name );
         recursive_command ( ctx.commands, 1);   
      }   */
      
      System.out.println ("\n--- Trying out commands ---");
      
      //trying out command and see if they get executed.
      
      BB.parse_input("show bonjour param1 param2");
      BB.parse_input("show hello param1 ");
      BB.parse_input("compute add 3 5");
      BB.parse_input("subranch show bonjour param1");
      BB.parse_input("This should generate an error");
      BB.parse_input("show another error");
      BB.parse_input("show  hello  param1");
      BB.parse_input("  show bonjour param1 param2  ");
      
      // Print out the result buffer
      System.out.println ("\n--- Console output ---");
      System.out.println( BB.out );
      
   }
   
   public static void terminal (IRuleBox rulebox)
   {
      BB.init( rulebox );
      
      //building the test command tree
      rulebox.application_startup();
      /*SContext gamectx = new SContext ("GAME");
      BB.context_list.add (gamectx);
      
      gamectx.commands.add ("show");
      gamectx.commands.add ("compute");
      gamectx.commands.add ("subranch");    
            
      gamectx.commands.child("show").add( new CmdBonjour());      
      gamectx.commands.child("show").add( new CmdHello());      
      gamectx.commands.child("compute").add( new CmdAdd());
      gamectx.commands.child("subranch").add ( gamectx.commands.child("show") );
      
      BB.active_context = gamectx;*/
      
      //---trying out commands ---
      
      //BB.parse_input("show bonjour param1 param2");
      //BB.parse_input("show hello param1 ");
      //BB.parse_input("compute add 3 5");
      //BB.parse_input("subranch show bonjour param1");
      //BB.parse_input("This should generate an error");
      //BB.parse_input("show another error");
      //BB.parse_input("show  hello  param1");
      //BB.parse_input("  show bonjour param1 param2  ");
      
      //BB.out.print ("\nAllo\nAllo\nAllo\nAllo\n\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo\nAllo");
      
      JFrame terminal = SwingTerminal.create();
      
   }
   
   public static void inbuffer(IRuleBox rulebox)
   {
      BB.init( rulebox );
      
      System.out.println (BB.in.get_previous());
      
      BB.in.put ("Allo");
      BB.in.put ("Bonjour");
      BB.in.put ("Comment");
      BB.in.put ("Ca va");
      
      for ( int i = 1; i < 5; i++)
      {
         String str = BB.in.get(i);
         if ( str != null) System.out.println(str);
      }
      
      System.out.println ("Prevous-Next");
      System.out.println ( BB.in.get_next());
      System.out.println ( BB.in.get_previous());
      System.out.println ( BB.in.get_previous());
      System.out.println ( BB.in.get_previous());
      System.out.println ( BB.in.get_next());
      System.out.println ( BB.in.get_next());
      System.out.println ( BB.in.get_next());
      System.out.println ( BB.in.get_next());
      System.out.println ( BB.in.get_previous());
      System.out.println ( BB.in.get_previous());
      BB.in.put ("Ca va bien");
      System.out.println ( BB.in.get_previous());
   }
   
   /**
    * This is used to compare the speed by using a tree to search for the
    * commands or using a simple hash map and search the whole string.
    * 
    * There might be some features that would require a sequential hashmap search.
    * @param rulebox
    */
   
   public static void speed_test (IRuleBox rulebox)
   {
      long time_start, time_end, time_diff;
      
      //----- Old Method -----
      
      BB.init( rulebox);
      
      rulebox.application_startup();
      //building the test command tree
      /*SContext gamectx = new SContext ("GAME");
      BB.context_list.add (gamectx);
      
      gamectx.commands.add ("show");
      gamectx.commands.add ("compute");
      gamectx.commands.add ("subranch");    
            
      gamectx.commands.child("show").add( new CmdBonjour());      
      gamectx.commands.child("show").add( new CmdHello());      
      gamectx.commands.child("compute").add( new CmdAdd());
      gamectx.commands.child("subranch").add (gamectx.commands.child("show") );
      
      BB.active_context = gamectx;*/
      
      String[] tokens = "subranch show bonjour".split(" ");
      
      
      
      time_start = System.currentTimeMillis();
      /*for ( int j = 0 ; j < 100000 ; j++)
      {
         
         //BB.parse_input("subranch show bonjour");
      
         SCommandTreeNode node = BB.active_context.commands;
         SCommandTreeNode newnode; // if not null, then found
         boolean stop = false;
         int i = 0;
         ICommand cmd = null;
                  
         //--- Search the command tree ---
         
         //valid node up to that point.
         if ( node != null )
         {
            //for each keyword typed            
            while ( i < tokens.length && stop == false  )
            { 
               //BB.out.println(tokens[i]);
               Iterator<SCommandTreeNode> itr = node.childrens().iterator();
               //search for a valid command
               //if ( node.childrens() != null )
               //{  itr = node.childrens().iterator();
               //}
               
               newnode = null;
               while ( itr.hasNext() && newnode == null )
               {  SCommandTreeNode tmpnode = itr.next();        
                  //BB.out.printf ("DEBUG: Comparing token %s with key %s\n",
                  //    tokens[i], tmpnode.name() );
                  if ( tokens[i].equals(tmpnode.name()))
                  {                       
                     newnode = tmpnode;    
                     //BB.out.printf("DEBUG: found %s\n", tokens[i]);
                  }
               }
               
               if (newnode != null) // we found a keyword
               { 
                  if ( newnode.is_leaf() == false)
                  {  //This is just a branch node, requires further digging
                     node = newnode; 
                  }   
                  else //valid command found, will need to show parameters
                  {
                     //BB.out.println ("DEBUG: Command found");
                     cmd = newnode.command();
                     //if ( cmd == null) BB.out.println ("ERROR: Command found pointer is somehow assigned to null");
                     stop = true;
                  }
               }
               else
               {
                  //BB.out.println ("DEBUG: Command NOT found, exiting loop");
                  stop = true;
               }
               
               i++;
            }
         }   
         //else BB.out.println ("ERROR: BB.parse_input: Node is null" ); 
         
         cmd.execute(null);
         //if (cmd != null) cmd.execute(null);
         //else BB.out.println ("ERROR: No Command Found" );
      }*/
      //System.out.print ( BB.out.toString());
      time_end = System.currentTimeMillis();
      time_diff = time_end - time_start;
      
      System.out.printf ("Old method delay: %d\n", time_diff);
      
      //----- new method -----
      
      Map<String, ICommand> cmdlist = new HashMap<>();
      
      //NOTE: I am not using the command name for now. Can have some issue
      //in putting the full path in the command name if the command is put at
      // different place in the hierarchy, Else put the path in the name
      cmdlist.put ("show bonjour", new CmdBonjour());
      cmdlist.put ("show hello", new CmdHello());
      cmdlist.put ("compute add", new CmdAdd());
      cmdlist.put ("subranch show bonjour", new CmdBonjour());
      cmdlist.put ("subranch show hello", new CmdBonjour());
      cmdlist.put ("subranch show onjour", new CmdBonjour());
      cmdlist.put ("subranch show njour", new CmdBonjour());
      cmdlist.put ("subranch show jour", new CmdBonjour());
      cmdlist.put ("subranch show our", new CmdBonjour());
      cmdlist.put ("subranch show ur", new CmdBonjour());
      cmdlist.put ("subranch show r", new CmdBonjour());
      cmdlist.put ("subranch show bonjou", new CmdBonjour());
      cmdlist.put ("subranch show bonjo", new CmdBonjour());
      cmdlist.put ("subranch show bonj", new CmdBonjour());
      cmdlist.put ("subranch show bon", new CmdBonjour());
      cmdlist.put ("subranch show bo", new CmdBonjour());
      cmdlist.put ("subranch show b", new CmdBonjour());
      cmdlist.put ("subranch show again hello", new CmdHello());
      
      time_start = System.currentTimeMillis();
      for ( int j = 0 ;  j< 100000 ; j++)
      {
         cmdlist.get ("subranch show again hello");//.execute(null);      
      }
      time_end = System.currentTimeMillis();
      time_diff = time_end - time_start;
      
      
      
      System.out.printf ("New method delay: %d\n", time_diff);
      
      time_start = System.currentTimeMillis();
      for ( int j = 0 ;  j< 100000 ; j++)
      {
         for ( String str: cmdlist.keySet())
         {
            //System.out.println ( "Key = " + str );
            if ( str.contains ("subranch show again hello" )) 
            //if ( str.contains ("compute" )) 
            {
               //System.out.println ("Found!");
               break;
            }               
         }
      }
      time_end = System.currentTimeMillis();
      time_diff = time_end - time_start;
      
      System.out.printf ("Cycle search delay: %d\n", time_diff);
      
      time_start = System.currentTimeMillis();
      for ( int j = 0 ;  j< 100000 ; j++)
      {
         for ( String str: cmdlist.keySet())
         {
            //System.out.println ( "Key = " + str );
            if ( "subranch show again hello param1 param2 param3".startsWith (str)) 
            //if ( str.contains ("compute" )) 
            {
               //System.out.println ("Found!");
               break;
            }               
         }
      }
      time_end = System.currentTimeMillis();
      time_diff = time_end - time_start;
      
      System.out.printf ("Reverse cycle search delay: %d\n", time_diff);
      
      
      
      //System.out.print ( BB.out.toString());
      
   }
   
   public static void launcher (IRuleBox rulebox)
   {
      SwingTerminalLauncher launcher = new SwingTerminalLauncher();
      launcher.start (rulebox);
      //System.out.println ("DEBUG: I am out of start()");  
      
      
      
      //TODO LAUNCHER SCRIPT load game script from file and test game.
      
   }
}
