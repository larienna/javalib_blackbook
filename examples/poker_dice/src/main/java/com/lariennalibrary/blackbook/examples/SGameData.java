/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.examples;


import com.lariennalibrary.blackbook.BB;
import java.util.HashMap;
import java.util.Map;

/**
 * Structure containing all the game information. COnstant data will also be 
 * placed in the save game structure but it will not need to be saved to a file.
 *  
 * Design Note: decided to make it a non static class even if there will be only
 * 1 instance of this class. The reason is because the JSON parser might require
 * getters, and setters. So a static class might not work.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class SGameData 
{
   /**Card Faces to be shown on the terminal when displayed*/
   public static final char[] FACES = { '9','T','J','Q','K','A','*'};
   
      
   /**Amount of money the player currently have*/
   public int money;
   /**The current bid of the player*/
   public int bid;
   /**Number of rerolls available before payout*/
   public int reroll_left; 
   /**Dices currently rolled*/
   public int [] dice;

   public SGameData ()
   {
      money = 1000;
      bid = 10;
      reroll_left = RuleBox.NB_REROLL; // there might be a game constant to change this.
      
      dice = new int [RuleBox.NB_DICE];
      for ( int i = 0 ; i < 5 ; i++) dice [i] = 6; //last symbol is reroll '*'      
   }
   
   /**
    * Returns a terminal friendly way to display the results of the rolls.
    * 
    * Design Note: Normally, a method like that should probably need to be put
    * into some sort of terminal printer.
    * 
    * @return A string withe the results of the roll
    */
   public String get_printed_rolls ()
   {
      StringBuilder str = new StringBuilder();
      
      for ( int i = 0 ; i < RuleBox.data.dice.length; i++)
      {
         str.append ('[');
         str.append(FACES [RuleBox.data.dice[i]]);
         str.append("] ");
      }
      
      return str.toString();
   }
   
   /**
    * Simple method that sorts the value of the dice using a slection sort.
    * 
    */
   
   public void sort_dice ()
   {  int i;
      int j;
      int swap;
      
      for ( i = 0 ; i < dice.length; i++ )
      {
         for ( j = i + 1 ; j < dice.length; j++)
         {
            if (dice[i] > dice[j])
            {
               swap = dice[i];
               dice[i] = dice[j];
               dice[j] = swap;
            }           
         }
      }
   }
}
