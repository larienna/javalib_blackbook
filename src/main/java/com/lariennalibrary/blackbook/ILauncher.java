/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

/**
 * This is the interface that should be implemented to launch a game via a 
 * rule box passed in parameters. The reason I am using the more complex abstract
 * approach is to isolate the code. For example SwingTerminalLauncher implementation
 * must not be called anywhere in the LibGDX version of the game. If I create a 
 * launching static method, it will have an instance on all launcher types which
 * is bad.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public interface ILauncher 
{
   /**
    * This is the method that must be called to start the game. It will generally
    * call the BB.init method and do other initialisation according to the
    * type of launcher used. It could exit after initialisation, it depends
    * on the type of launcher.
    * 
    * @param rulebox this is the game that must be passed in parameter    
    */
   public void start ( IRuleBox rulebox);
   
   /**
    * This method is called when Black Book Requires to close the application.
    * It can come from in game, or during the INIT context by typing an "exit"
    * command for example. The closing method will change from a platform to
    * another.
    */
   //public void exit ();
   //NOTE: Maybe all applications are closed with System.exit(0), so no need to
   //keep a reference to the launcher in BB
   
   
   
}
