/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

/**
 * This class encpsulate StringBuilder to create a buffer for the terminal. It
 * as the feature of auto deleting ancient output over a certain limit. The
 * buffer cannot overflow befause StringBuilder prevent that. Terminal 
 * application will simply need to add the content of the buffer in their 
 * interface.
 * 
 * The trunking of the buffer will be made according to the number of characters
 * and not the number of lines. So partial lines could be cut out.
 * 
 * Design Notes: This is one of the few class that does not follow NOOP since it
 * is somewhat a kind of data structure in it self. So data and code is required.
 * Still all the members are immutable even if the StringBuilder content change.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class TerminalOutBuffer 
{
   
   
   //private final float _truncate_percent;
   
   /**This is the currently line currently beign build. A new one is built
    after each line*/
   private StringBuilder _str;
   /**Now use a rotating buffer of string pointers*/
   private final String[] _buffer;
   /**Insertion point of the new string. Should point on null or next str to remove*/
   private int _index;
   //private final int _capacity;
   
   /**
    * Creates a new terminal buffer. You can set a maximum size in lines. 
    * 
    * @param capacity Initial size of the buffer, but also where the buffer will
    * be truncated.
    
    */
   public TerminalOutBuffer ( int capacity )
   {
      //_capacity = capacity;
      _str = new StringBuilder();
      _buffer = new String[capacity];
      _index = 0;
      
      //--- old code ---
      //System.out.println("Youhou!Bonjour");
      //_str = new StringBuilder (capacity);
      //System.out.printf("TerminalOutBuffer.init() capacity is %d", _capacity);
      
      //System.out.printf("\nTerminalBuffer.init() parameter capacity is %d", capacity);
      //System.out.printf("\nTerminalBuffer.init() capacity is %d", _capacity);
      
      /*if ( truncate_percent >= 0 && truncate_percent <=1 )
      {
         _truncate_percent = truncate_percent;
      }
      else
      {
         _truncate_percent = 0.25f;
      } */             
   }
   
   /**
    * Takes each line in the buffer and append it to a StringBuilder that will
    * return a final string for the whole buffer.
    * 
    * @return string containing the whole buffer
    */
   
   @Override
   public String toString ()
   {
      StringBuilder builder = new StringBuilder();
      
      //if there is anything not in the buffer insert it
      if (_str.length() > 0) flush(); 
      
      //DEBUG
      //builder.append("Current index is:");
      //builder.append(_index);
      //builder.append('\n');
      //rotating around the buffer in reverse order (old to new)
      int i = _index;
      do 
      {  if ( _buffer[i] != null )
         {  //builder.append ( i ); //DEBUG
            builder.append ( _buffer[i]);            
         }
      
         i++;
         i = Math.floorMod(i, _buffer.length); // do a modulo for the rotation
         //if (i >= _buffer.length) i = 0;                 
      }
      while ( i != _index );       
      
      return builder.toString();
   }   
   
   public void print(boolean b) 
   {  _str.append (b);           
   }

   public void 	print(char c)
   {  if ( c == '\n') flush();      
      else _str.append(c);
   }

   public void 	print(char[] s)
   {  insert_tokens (new String (s) );
   }

   public void 	print(double d)
   {  _str.append (d);     
   }

   public void 	print(float f)
   {  _str.append (f);      
   }

   public void 	print(int i)
   {  _str.append (i);
   }

   public void 	print(long l)
   {  _str.append (l);      
   }

   public void 	print(Object obj)
   {  insert_tokens (obj.toString());      
   }

   public void 	print(String s)           
   {  insert_tokens (s);      
   }

   public void 	printf(String format, Object... args)
   {  insert_tokens ( String.format ( format, args ));    
   }

   public void 	println()
   {  flush();
   }

   public void 	println(boolean x)
   {  _str.append (x);
      flush();
   }

   public void 	println(char x)
   {  
      if ( x == '\n') flush();
      else _str.append (x);
      flush();
   }

   public void 	println(char[] x)
   {  insert_tokens ( new String (x));
      flush();      
   }

   public void 	println(double x)
   {  _str.append (x);
      flush();
   }

   public void 	println(float x)
   {  _str.append (x);
      flush();
   }

   public void 	println(int x)
   {  _str.append (x);
      flush();
   }

   public void 	println(long x)
   {  _str.append (x);
      flush();
   }

   public void 	println(Object x)           
   {  insert_tokens ( x.toString());     
      flush();
   }

   public void 	println(String x)
   {  insert_tokens (x);      
      flush();
   }
   
   //----- Private Methods -----
   
   
  /* private void truncate ()
   {
      int nb_char;
      //System.out.printf("The length is %d and the capacity is %d", _str.length(), _capacity);
      if ( _str.length() > _capacity )
      {
         nb_char = _str.length() - _capacity;
         nb_char += _capacity * _truncate_percent;
         //System.out.printf("\nTrying to trucate %d characters", nb_char);
         _str.delete ( 0, nb_char );
      }
   }*/
   
   /**
    * Ends the last line of the output with a \n and sends it into the buffer
    */
   
   /*public void flush ()
   {
      _str.append ('\n');
      insert();
   }*/
   
   /**
    * Convert the current output string into a final string and terminate it
    * with a \n, adds it to the buffer and create a new string builder.
    * 
    */
   public void flush ( )
   {
      _str.append('\n');
      _buffer [ _index] = _str.toString();
      _str = new StringBuilder();
      _index++;
      _index = Math.floorMod(_index, _buffer.length); //do a modulo for the rotation
      //if ( _index >= _buffer.length) _index = 0;      
      
   }
      
   
   /**
    * This method will take a string and split it on the \n characters to ensure
    * that each string separated by a \n will go on a different line in the 
    * buffer. Then each of those lines will be inserted into the buffer.
    * 
    * @param tmpstr String to split and analyse
    */
   
   private void insert_tokens (String tmpstr)
   {
      String []tokens = tmpstr.split("\\n");
      Boolean token_was_inserted = false;
      
      for ( String tok: tokens)
      {
         if ( token_was_inserted )
         {            
            flush();            
         }
         _str.append(tok);
         token_was_inserted = true;         
      }
      
      //Exception case where a training \n seems to be ignored by split
      if ( tmpstr.charAt( tmpstr.length()-1) == '\n') flush();
   }
}
