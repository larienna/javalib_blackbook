#!/bin/bash
GAMENAME="poker_dice"
#version number of the library here. Must be updated after each release
VERSION="0.2.0"

echo "----------- Building the software --------------"
gradle :examples:$GAMENAME:build

#running the demo
#echo "----------- Starting the demo --------------"
gradle :examples:$GAMENAME:run


#cd examples
#rm *.jar
#cd ..

#copying necessary libraries
#cp build/libs/javalib_blackbook-$VERSION.jar ./examples
#cp blackbook_terminal/build/libs/blackbook_terminal.jar ./examples

#TODO do not hardcode example name, try to use CL parameters to run various examples
#cp examples/$GAMENAME/build/libs/$GAMENAME.jar ./examples
#cd examples
#java -jar $GAMENAME.jar Launcher
#java -jar $GAMENAME.jar





