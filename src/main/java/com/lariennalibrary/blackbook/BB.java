/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * This is the main blackboard where all the drivers and global variables are
 * set. 
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public final class BB 
{
   //TODO ?INSTANCIFY BB class: In order to be able to use a constructor to have
   //only final fields, it could be possible to put many of this data into a 
   //separate class and hold instead a static reference to this new class that
   //is somewhat a singleton. The main feature is the extra protextion on more 
   //final fields. Still it creates a proliferation of class syndrome
   //As singleton, need to add "instance" indentifier. BB.instance.out is long.
         
   /**Version number of the library*/
   public static final String VERSION = "0.2.0 (2019)";
   
   /**First terminal buffer */
   public static TerminalOutBuffer left_out;// = new TerminalOutBuffer(500);
   //NOTE: made initialisation in init and unfinalised it to avoid adroid static bug
   /**Second terminal buffer*/
   public static TerminalOutBuffer right_out; //= new TerminalOutBuffer(500);
   
   /**Current Output buffer to use. It can be changed according to command
    * buffer redirection. This reference will point on the left or right buffer.
    * The game designer does not need to consider it, and use only this reference.
    */
   public static TerminalOutBuffer out; // = left_out;
   
   /**Input buffer which basically hold the last inputed command*/
   public static TerminalInBuffer in;// = new TerminalInBuffer(50);
   
   /**Reference on the game box for calling additional methods. Normally all the
    * commands should have been registered within the context, and 
    * application_startup() should have already been called after creation.    * 
    */
   public static IRuleBox rulebox;
   
   /**A reference on the launcher to call the exit method when necessary*/   
   //public static ILauncher launcher;
   
   /**List of context and command trees*/
   //public static ArrayList<SContext> context_list;
   
   /**Current prefix*/
   public static String prefix = "";
   
   /**Current active context name*/
   //public static SContext active_context;
   private static String _context_name;
   
   /**List of command libraries that could contains potential commands */
   public static ArrayList<ICommandLibrary> command_library;
   
   /**Master list of commands that will be used by the parser*/
   private static Map<String, ICommand> _command_list;
   
   /**Master list of commands stored as tokens for the context help*/
   //private static ArrayList<String[]> _command_tokens;
   
   /**Command to be repeated on the right terminal after each new command*/
   private static ICommand _repeat_command;
   /**Array of parameters sent with the repeating command*/
   private static ArrayList<String> _repeat_parameters;
      
   /**
    * Initialisation method to call at the start of the game to make sure every
    * thing is in place to start the game.
    * 
    * @param rulebox game rules to be used 
//    * @param launcher launcher that was used to start the blackbook
    */
   
   //TODO: Maybe add drivers here to force programmers to set them in the first
   //place. Make them final variables to avoid changing the driver. Can still 
   //pass null driver if not used.   
   //The drivers could move up to the launcher, as this is what the user calls.
   
   //TODO DRIVER/MODULE LIST maybe set a list of modules, add to a list and
   //pass in parameter to init. Each module will print a message on the
   //terminal during startup.
   
   public static void init ( IRuleBox rulebox/*, ILauncher launcher */)
   {
      //setting up terminal buffers
      in = new TerminalInBuffer(50);
      right_out = new TerminalOutBuffer(500);
      left_out = new TerminalOutBuffer(500);
      out = left_out;
      
      //building up the command list
      
      command_library = new ArrayList<>();
      _command_list = new HashMap<>();
      //_command_tokens = new ArrayList<>();
      
      command_library.add (rulebox);
      
      set_context ("INIT");
               
      
      //adding default contexts present in all game
      
      /*context_list = new ArrayList<>();
      SContext initctx = new SContext ("INIT");
      context_list.add ( initctx );
      context_list.add (new SContext ("SETUP"));
      context_list.add (new SContext ("ENDGAME"));*/
      
      BB.out.println ("Welcome!\n");
      BB.out.println ("This game is powered by");
      BB.out.println ("Black Book [|]");
      BB.out.printf  ("Version %s\n", BB.VERSION);
      
      BB.out.printf  ("\n%s", rulebox.gameinfo() );
      
            
      BB.right_out.println ("Type your command then enter to execute it");      
      BB.right_out.println ("Type an empty command to show available commands");
      BB.right_out.println ("Type incomplete command to show available sub commands");
      BB.right_out.println ("Use tab to complete commands when there is only 1 possible keyword\n");
      BB.right_out.println ("End you command with one of those operators for special effects");
      BB.right_out.println ("> : Display command on the right terminal");
      BB.right_out.println ("@ : Repeat command on the right terminal until the context is changed");
      BB.right_out.println ("? : Display the help of the command");
      BB.right_out.println (". : Set a branch node as a prefix for next commands");
           
      
      _repeat_command = null;
      _repeat_parameters = null;
      
      BB.rulebox = rulebox;
      //BB.launcher = launcher;
   }
      
   
   /**
    * Parse the command line when the user press enter and launch the appropriate
    * commands with parameters. This is not called for context sensitive 
    * incomplete commands
    * 
    * @param cli_input string input by the user.
 
    */
   public static void parse_input ( String cli_input )
   {      
      ArrayList<String> param;
      ArrayList<String> cmd_found = new ArrayList<>();
      //ICommand cmd = null; // if not null, then found
      char operator; //special character inserted at the end of commands
      int nb_cmd_found = 0; //number of commands found during the search
      //StringBuilder cmd_found = new StringBuilder();
      ICommand last_command = null;
      String last_cmd_name = "";
      String input;
      //String cmd_prefix = "CMD: "; 
      //StringBuilder errstr_valid = new StringBuilder(); //not sure since not cumulative
      
      //removing useless whitespace from input string 
      cli_input = cli_input.replaceAll("\\s{2,}", " ").trim();
      
      //extract and trim the operator character if present
      input = cli_input;
      operator = get_special_operator (cli_input);      
      if ( operator != 0 ) input = trim_last_token ( cli_input );
      
      
      //search the command list for a matching command prefix
      for ( Entry<String, ICommand> entry: _command_list.entrySet())
      {
         //Both conditions are used in case one string is bigger than the other
         // or vice versa.
         if ( input.startsWith(entry.getKey()) 
           || entry.getKey().startsWith(input) )
         {
            nb_cmd_found++;
            //cmd_found.append (cmd_prefix);
            cmd_found.add (entry.getKey());
            //cmd_prefix = "\n     ";
            
            if ( input.startsWith(entry.getKey() ))
            {
               last_command = entry.getValue();
               last_cmd_name = entry.getKey();
            }
         }
      }
      
      
      //swith the buffer to display to the right
      if ( operator == '>' || operator == '@') BB.out = BB.right_out;
      
      //Interpret command
      BB.out.printf ( "IN : %s\n", cli_input);
      BB.in.put (cli_input);          
      if (nb_cmd_found == 0) // no command found
      {         
         BB.out.printf ( "ERR: Command not found\n");
      }
      else if (nb_cmd_found > 1 || last_command == null) //Too many command found
      {  //or no valid commands found
         if ( operator == '.' )
         {  //Build the prefix for the current branch               
               prefix = input + " "; // assume input is smaller, justify finding many
               BB.out.printf ("MSG: Changing prefix to : %s\n", prefix);               
         }
         else print_sorted_command_found (cmd_found);
      }
      else //the right command has been found
      {
         if ( operator == '.' ) BB.out.println ("MSG: Ignoring . operator since command the was found");
                  
         //reset the repeat commands if displaying on the right buffer
         if ( operator == '>' || operator == '@')
         {  
            _repeat_command = null;
            _repeat_parameters = null;
         }
         
         param = separate_parameters ( input.substring ( last_cmd_name.length() ) );
         
         if ( operator == '?' )
         {  //display the help instead of running the command
            BB.out.println( last_command.help());
         }
         else
         {  //Run the command
            //save the string into the input buffer if the command is valid
            
            last_command.execute(param);    
            if ( _repeat_command != null )
            {  // repeat the command on the right terminal
               BB.out = right_out;
               _repeat_command.execute(_repeat_parameters);     
            }
         }        
         
         if ( operator == '@')
         {  //save the command and the parameters to repeat it later
            _repeat_command = last_command;
            _repeat_parameters = param;            
         }       
         
         //BB.out.print ("DEBUG: The parameter list was: ");
         //BB.out.println (param.toString() );         
      }     
      
      //reset the default terminal on the left
      BB.out = left_out;
      
      
   }
   
   /**
    * This private function will strip the last token from a string, generally
    * usefull to get rid of trailing operators. It is recommended to call
    * get_special_operator first to see if trimming is necessary
    * 
    * @param input command line input
    * @return return the command string without the last token
    */
   private static String trim_last_token ( String input )
   {
      StringBuilder str = new StringBuilder();
      String[] tokens;
      int i;
      
      tokens = input.split(" ");
      
      for ( i = 0 ; i < tokens.length - 1; i++ ) 
      {
         str.append (tokens[i]);
         if ( i != tokens.length - 2 ) str.append (" ");
      }            
      
      return str.toString();
   }
   
   /**
    * This function will return the special operator character at the end of
    * the input string
    * @param input
    * @return 
    */
   private static char get_special_operator ( String input )
   {
      char operator = 0;
      char tmpchar;
      String[] tokens;
      
      tokens = input.split(" ");
      
      if ( tokens[tokens.length - 1] != null && tokens[tokens.length - 1].length() > 0)
      {
         tmpchar = tokens[tokens.length - 1].charAt(0);          
         if ( tmpchar == '>' || tmpchar == '@' || tmpchar == '?' || tmpchar == '.' ) 
         {
            operator = tmpchar;         
         }
      }  
      
      return operator;
   }
   
   /**
    * This method is used to cut the remaining of the command input into parameters
    * 
    * @param input right portion of the input string after the command name
    * @return an Arraylist of string tokens
    */
   

   private static ArrayList<String> separate_parameters ( String input )
   {
      String[] tokens;
      ArrayList<String> param = new ArrayList<>();
      
      input = input.trim();      
      tokens = input.split(" ");
      
      for (String token : tokens)
      {
         param.add(token);           
      }
      
      return param;
   }
   
   /**
    * Sort an array list of string contaning the commands found and print it
    * on the screen.
    * @param list 
    */
   
   private static void print_sorted_command_found ( ArrayList<String> list)
   {  
      Collections.sort (list);
      String cmd_prefix = "CMD: ";
      StringBuilder strbuilder = new StringBuilder();
      
      for ( String str: list)
      {
         strbuilder.append (cmd_prefix);
         strbuilder.append (str);
         cmd_prefix = "\n     ";
      }
      
      BB.out.println ( strbuilder.toString());
      
   }
   
   //OBSOLETE ancient parser with tree structure
   /*public static void parse_input ( String cli_input )
   {
      String[] tokens;
      int toklen; // length of the token to introduce white space in the error line
      ArrayList<String> param = new ArrayList<>();
      boolean err = false;
      //boolean keyword_found = false;
      //boolean command_found = false;
      //Keywords accepted before the error
      StringBuilder errstr_valid = new StringBuilder(); 
      //Keywords rejected after the error
      StringBuilder errstr_invalid = new StringBuilder();
      SCommandTreeNode node = active_context.commands;
      SCommandTreeNode newnode = null; // if not null, then found
      ICommand cmd = null; // if not null, then found
      char operator = 0; //special character inserted at the end of commands
      
      //removing useless whitespace from input string 
      cli_input = cli_input.replaceAll("\\s{2,}", " ").trim();
      
      tokens = cli_input.split(" ");
      errstr_valid.append ("CMD: ");
      errstr_invalid.append ("ERR: ");
      
      int nb_of_tokens = tokens.length; //can be modified if special commands are used
      //BB.out.printf ("DEBUG: Testing this command: %s\n", cli_input);
      
      //--- Interpret special functions ---
      //get the first character of the last command
      //if ( tokens.length > 0 )
      //{
      //   System.out.printf ("DEBUG: tokens.length = %d", tokens.length);
      // check if there is a valid string to interpret to avoid exceptions.
      

      if ( tokens[tokens.length - 1] != null && tokens[tokens.length - 1].length() > 0)
      {
         char tmpchar = tokens[tokens.length - 1].charAt(0);          
         if ( tmpchar == '>' || tmpchar == '@' || tmpchar == '?' || tmpchar == '.' ) 
         {
            operator = tmpchar;
            //cut the final element from the length
            nb_of_tokens--;
         }
      }  

      //--- Search the command tree ---
      if ( node != null )
      {
         //for each keyword typed
         int i = 0;
         while ( i < nb_of_tokens && err == false && cmd == null )
         {
            //debug : show all tokens
            //BB.out.println(tokens[i]);
            //if ( cmd != null) BB.out.println ("ERROR: I should not be displayed");

            Iterator<SCommandTreeNode> itr = null;
            //search for a valid command
            if ( node.childrens() != null )
            {  itr = node.childrens().iterator();
            }
            else BB.out.println ("ERROR: There is no childrens in this node");

            newnode = null;
            // search on the current node if the token command exist
            while ( itr != null && itr.hasNext() && newnode == null )
            {
               SCommandTreeNode tmpnode = itr.next();
               //BB.out.printf ("DEBUG: Comparing token %s with key %s\n",
               //        tokens[i], entry.getKey() );
               //String key = entry.getKey();
               if ( tokens[i].equals(tmpnode.name()))
               {  

                  errstr_valid.append(tokens[i]);
                  errstr_valid.append(' ');
                  toklen = tokens[i].length() + 1;

                  //fill with white space the invalid line to make them alligned
                  for ( int j = 0; j < toklen; j++ ) errstr_invalid.append(' ');

                  newnode = tmpnode;
                  //BB.out.printf("DEBUG: found %s\n", tokens[i]);
               }
            }


            if (newnode != null) // we found a keyword
            {  if ( newnode.is_leaf() == false)
               {  //This is just a branch node, requires further digging
                  node = newnode;                  
               }
               else
               {  //BB.out.println ("DEBUG: Command found");
                  cmd = newnode.command();              
                  if ( cmd == null) BB.out.println ("ERROR: Command found pointer is somehow assigned to null");
                  //Add remaining tokens as parameters
                  for ( int j = i + 1; j < tokens.length; j++)
                  {  param.add (tokens[j]);                     
                  }  
                  
               }
            }   
            else // the keyword was never found
            {  err = true;
               //BB.out.println ("DEBUG: Command NOT found");
               errstr_invalid.append('*');
               //append remaining tokens
               for ( int j = i; j < nb_of_tokens; j++)
               {
                  errstr_invalid.append (tokens[j]);
                  errstr_invalid.append (' ');
               }                  
            }

            i++;
         }
      } 
      else BB.out.println ("ERROR: BB.parse_input: Node is null" );   
      
      //append the operator to the valid or invalid string

      if ( operator != 0 )
      {  if ( err == false  ) errstr_valid.append ( operator );
         else errstr_invalid.append ( operator );         
      }

      
      if ( cmd == null )
      {  //There was no error but no command was found, so it should be a branch node
         BB.out.println (errstr_valid.toString() );
         
         
         if ( err == false)
         {
            if ( operator == '.' )
            {  //Build the prefix for the current branch
               StringBuilder prefixbld = new StringBuilder();
               for ( int i = 0 ; i < nb_of_tokens ; i++)
               {  prefixbld.append ( tokens[i]);
                  prefixbld.append (' ');
               }
               prefix = prefixbld.toString();
               BB.out.printf ("Changing prefix to : %s\n", prefix);
               
            }
            else
            {
               BB.out.println ("No command was found, this is a branch node");
               BB.out.println ("You can use the '.' operator to set this branch as a prefix");
            }
         }
         else BB.out.println (errstr_invalid.toString() ); //command is invalid
         
         
         //BB.out.print ("DEBUG: The token list was: ");
         //BB.out.println (Arrays.toString(tokens) );
      }   
      else // the command was found and no error occured, so run the command
      {
         //BB.out.printf("DEBUG: launching command %s with param\n", cmd.name());  
         if ( operator == '>' || operator == '@')
         {  //swith the buffer to display to the right
            BB.out = BB.right_out;
            _repeat_command = null;
            _repeat_parameters = null;
         }
         
         BB.out.println ( errstr_valid.toString());
         if ( operator == '?' )
         {  //display the help instead of running the command
            BB.out.println( cmd.help());
         }
         else
         {  //Run the command
            //save the string into the input buffer if the command is valid
            BB.in.put (cli_input);          
            cmd.execute(param);    
            if ( _repeat_command != null )
            {  // repeat the command on the right terminal
               BB.out = right_out;
               _repeat_command.execute(_repeat_parameters);
               BB.out = left_out;
            }
         }
         
         if ( operator == '>' || operator == '@')
         {  //reset the out buffer to the left terminal
            BB.out = BB.left_out;
            if ( operator == '@')
            {  //save the command and the parameters to repeat it later
               _repeat_command = cmd;
               _repeat_parameters = param;
            }
         }
         
         
         //BB.out.print ("DEBUG: The parameter list was: ");
         //BB.out.println (param.toString() );
      }
      
      
   }*/
   
   /**
    * get the current context name. Only a getter.
    * @return name of the context
    */
   
   public static String get_context ()
   {
      return _context_name;      
   }
   
   /**
    * Change the context of the application which should change the command list.
    * The command libraries should only return commands appropriate for that context.
    * 
    * @param name of the context. 
    */
   
   public static void set_context ( String name )
   {
      _context_name = name;
      
      _command_list.clear();
      //_command_tokens.clear();
            
      //Iterate over all the registered command libraries and add the commands
      //returned by the library.
      for ( ICommandLibrary library: command_library)
      {
         _command_list.putAll(library.get_commands(name));
      }
      
      
      // disble command repeat when changing context to avoid invalid commands
      // getting executed
      _repeat_command = null;
      _repeat_parameters = null;
      
      
      //make sure the commands are ordered alpabetically.
      //NOTE: hashmaps cannot be sorted, it needs to be manually done.
      
      
      //Generate the command tokens

/*      for ( String str: _command_list.keySet())
      {
         _command_tokens.add ( str.split(" "));
      }      */
   }
   
   /**
    * Use the currently inputed text on the command line to return context
    * sensitive help about the potential commands that could be typed.
    * 
    * @param input current command line
    * @return The help to display on the screen as you type.
    */
   //TODO OPTIMIZE since it's run after each key typed.
   //prevent garbage collecting, avoid unnecessary loops.
   public static String get_context_sensitive_help (String input)
   {

      String[] inputokens;
      StringBuilder strbld = new StringBuilder();
      StringBuilder common_input = new StringBuilder(); // a tmp string to find common match
      ArrayList<String> found_command;
      String[] cmd_tokens;
      int tokenid;
      Set<String> token_toprint = new HashSet<>();
      boolean all_matched;
      
      //strip out white space for tokenisation
      input = input.replaceAll("\\s{2,}", " ").trim();      
      inputokens = input.split(" ");
      
      //DEBUG
      //System.out.println (Arrays.toString(inputokens));
      //System.out.println (input);
      //Strip out the special operators if present      
      //could have some performance issue, see if necessary
      //if ( get_special_operator (input) != 0 ) input = trim_last_token ( input );
      
      strbld.append ( _context_name );
      strbld.append ( " : " );
      
      //search for valid commands with a valid prefix
      found_command = find_matching_commands ( input );
      
      //TODO SORTING the commands are sorted, but the tokens will not be sorted when displayed
      //the problem is that sets are not sorted, therefore the token list will have to be
      //converted into an arraylist first, then be sorted. NOt sure of worth it for the speed.
      //Collections.sort ( found_command );
      
      //Find commands currently typed that are common to all commands found so far
      tokenid = 0;
      all_matched = true;
      while ( all_matched && tokenid < inputokens.length)
      {  
         //create a string to compare the available commands with
         common_input.append (inputokens [ tokenid ]);
         common_input.append (' '); //prepare for the next command   

         for ( String cmd: found_command)
         {
            if ( cmd.startsWith ( common_input.toString()) == false) all_matched = false;
         }

         if ( all_matched == true )
         {
            //common_input.append (' '); //prepare for the next command   
            //build up the result into the final output string
            strbld.append (inputokens [ tokenid ]);
            strbld.append (' ');
         }
         tokenid++;
      }
      
      //DEBUG
      /*System.out.println ( "Found commands are:" );
      for ( String cmd: found_command)
      {
         System.out.println ( cmd );
      }*/
      
      
      
      //analyse the current token of each command to see if there is a match
      //Assume that the last token in the input string is the current token
      tokenid = inputokens.length - 1;
      if ( tokenid < 0 ) tokenid = 0;
      //if ( found_command.size() > 1 ) //there is more than 1 valid command available
      //{   
         for ( String cmd: found_command)
         {  //generate command tokens on the fly, could be slow on root of structure
            cmd_tokens = cmd.split (" ");
            //System.out.printf ("DEBUG: nb cmd tokens=%d, tokenid=%d\n"
            //        , cmd_tokens.length, tokenid+1);
            
            if ( cmd_tokens.length > tokenid+1 
                    && inputokens [ tokenid ].equals (cmd_tokens [tokenid] ) )                    
            {  //keyword is valid watch next keyword
               token_toprint.add ( cmd_tokens [tokenid+1]);                         
            }
            else if ( cmd_tokens.length > tokenid )
            {  //condition required to avoid indexoutof bound when param are inserted
               token_toprint.add ( cmd_tokens [tokenid]);
            }  
         }
         
         
      //NOTE does the same thing that the code above, but I also need the token
      //id which I cannot return. So I decided to duplicate the code.
      //the method below will be called by tab completion instead.
      //token_toprint = get_tokens_toprint ( inputokens, found_command );   
        
      //}
      
      //TODO COMMAND ANALYSIS SIMPLIFICATION maybe when parameters will be there
      //you could generate command with parameters in the list reducing the amount
      //of exception to consider and simplifying the system.
      //Will be applicable to the context sensitive help and the input parsing
      
      boolean draw_tokens = true;
      if ( found_command.size() == 1 ) 
      {  
         String cmd = found_command.iterator().next();         
         if ( input.startsWith (cmd))
         {  // the command is found, can start showing parameters         
            cmd_tokens = cmd.split (" ");
            //this is another failsafe to avoid an indexoutofbound when parameters are typed.
            if ( tokenid >= cmd_tokens.length) tokenid = cmd_tokens.length-1;
      
            if ( inputokens [ tokenid ].equals (cmd_tokens [tokenid] )) 
            {
               strbld.append ( cmd_tokens [tokenid] );
               strbld.append ('\n');
               //append the help string, could be slow as you need to research the list
               strbld.append ( _command_list.get( cmd ).help() );
               draw_tokens = false;
               //TODO PARAMETERS Show list of parameters when available
            }
         }
      }
      
      //draw tokens if the command is not found
      if ( draw_tokens == true )
      {
         strbld.append('\n');
         for ( String tok: token_toprint )
         {
            strbld.append ( tok );
            strbld.append (' ');
         }
      }
      
      return (strbld.toString());
   }
   
   /**
    * Simply search the list of commands for potential match.
    * 
    * @param input A cleaned up input string from the user
    * @return a list of valid commands
    */
   
   private static ArrayList<String> find_matching_commands ( String input )
   {
      ArrayList<String> found_command = new ArrayList<>();
      
      for ( String cmd: _command_list.keySet())
      {
         if ( input.startsWith(cmd) || cmd.startsWith(input) )
         {
            found_command.add ( cmd);
         }
      }
      
      return found_command;
   }
           
   /**
    * Returns are the keywords suggestion that should be printed ont the help
    * box according to valid commands that could be typed.
    * 
    * @param inputokens list of tokens from the input box
    * @param found_command list of potential commands found
    * @return list of tokens to display to the user
    */
   
   private static Set<String> get_tokens_toprint ( String[] inputokens,
           ArrayList<String> found_command )
   {
      Set<String> token_toprint = new HashSet<>();
      int tokenid = inputokens.length - 1;
      String[] cmd_tokens;
      
      if ( tokenid < 0 ) tokenid = 0;     
      
      for ( String cmd: found_command)
      {  //generate command tokens on the fly, could be slow on root of structure
         cmd_tokens = cmd.split (" ");
         if ( inputokens [ tokenid ].equals (cmd_tokens [tokenid] )
                 && cmd_tokens.length > tokenid+1) 
         {  //keyword is valid watch next keyword
            token_toprint.add ( cmd_tokens [tokenid+1]);                         
         }
         else token_toprint.add ( cmd_tokens [tokenid]);  
      }
      
      //TODO SORTING could be done here with, but not sure about the speed.
      
      return token_toprint;
   }
   
   //First attempt at making the method, did not work well, had sorting issues
   /*public static String get_context_sensitive_help ( String input )
   {
      StringBuilder strbld = new StringBuilder();
      int tokenid = 0;
      Set<String> matching_tokens = new HashSet<>();
      ArrayList<String[]> available_commands = new ArrayList<>();
      Set<String> available_tokens = new HashSet<>();
      String[] inputokens;
      boolean found_token;
      
      //strip out white space for tokenisation
      input = input.replaceAll("\\s{2,}", " ").trim();      
      inputokens = input.split(" ");
      
      //DEBUG
      System.out.println (Arrays.toString(inputokens));
      //Strip out the special operators if present      
      //could have some performance issue, see if necessary
      //if ( get_special_operator (input) != 0 ) input = trim_last_token ( input );
      
      
      
      strbld.append ( _context_name );
      strbld.append ( " : " );
      
      //Search for valid tokens already typed
      do
      {
         found_token = false;
         for ( String[] toklist: _command_tokens )
         {  // prevent overflow
            //System.out.println ("DEBUG: Looping");
            if ( tokenid < toklist.length ) 
            {
              // System.out.println ("DEBUG: entered the first condition");
               //if ( input.startsWith(tok [tokenid]) ) 
               if ( tokenid < inputokens.length )
               {  //add only keywords that matches the input. 
                  System.out.printf ("DEBUG: Comparing '%s' with '%s'\n", 
                          toklist[tokenid],inputokens[tokenid]);
                  if ( toklist[tokenid].equals (inputokens[tokenid]) )
                  {
                     matching_tokens.add ( toklist [tokenid] );               
                     found_token = true;
                     System.out.printf ("FOUND: %s\n", toklist[tokenid]);
                     available_commands.add ( toklist );
                  }
               }
               
            }
         }                  
           
         if ( found_token == true ) tokenid++;
      }
      while ( found_token == true );
            
      
      //Special condition where nothing is typed, so the all 
      //commands needs to be shown
      if ( tokenid == 0)
      {
         for ( String[] toklist: _command_tokens )
         { 
            available_commands.add ( toklist );
         }
      }
      
      //Draw all matching tokens
      for ( String tok: matching_tokens)
      {
         strbld.append (tok);
         strbld.append (' ');
      }
      
      strbld.append ('\n');
      
      //the current token id is not valid, so search for potential keywords to be typed
      //from the list of sub command available according to the previously typed commands
      //the tokenid position from the previous loop is kept.
      for ( String[] toklist: available_commands )
      {  // prevent overflow
         if ( tokenid < toklist.length ) 
         {  //allow filtering publicates when using a Set
            available_tokens.add ( toklist[tokenid] );
         }
      }     
      
      for (String tok: available_tokens )
      {

         //certain options are *command*, [command], (command)
         strbld.append ( tok );
         strbld.append ( ' ' );               
         
      }
      
      
      return strbld.toString();
   }*/
   
   /**
    * Method that will analyse the current input and return a different string
    * if a match could be found or the same string if no matching is found.
    * 
    * @param input the input of the user
    * @return the new text that should replace the user input.
    */
   
   public static String get_tab_completion ( String input )
   {
      StringBuilder strbld = new StringBuilder();
      String[] inputokens;
      ArrayList<String> found_command;
      Set<String> tokens_tomatch;
      int tokenid;
      
      input = input.replaceAll("\\s{2,}", " ").trim();      
      inputokens = input.split(" ");
      
      found_command = find_matching_commands ( input );
           
      tokens_tomatch = get_tokens_toprint ( inputokens, found_command );
      
      //at this point, I should have all the token the user should be able to type
      //If there is only 1 token, then we have a match and can complete
      if ( tokens_tomatch.size() == 1 )
      {  //need to build the new input string
         tokenid = inputokens.length -1;
         if ( tokenid < 0 ) tokenid = 0; // prevent overflow
         
         // if tokenid=0
         for ( int i = 0; i < tokenid; i++)
         {  // append already typed keywords
            strbld.append ( inputokens [ i ]);
            strbld.append (' ');     
            //DEBUG
            //System.out.print ( "Appending keyword ");
            //System.out.println ( inputokens [i] );
         }
         
         //append found new keyword
         strbld.append ( tokens_tomatch.iterator().next() );  
         strbld.append (' ');
         //DEBUG
         //System.out.print ( "Appending keyword ");
         //System.out.println ( tokens_tomatch.iterator().next() );
         
         return strbld.toString();
      }
      else return input; // there is no tab completion possible
      
      
      
      
   }
   
}
