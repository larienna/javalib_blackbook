package com.lariennalibrary.blackbook.examples;

import com.lariennalibrary.blackbook.examples.RuleBox;
import com.lariennalibrary.blackbook.terminal.SwingTerminalLauncher;


public class Launcher
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {
       SwingTerminalLauncher launcher = new SwingTerminalLauncher();
       launcher.start (new RuleBox());
 
    }
}
