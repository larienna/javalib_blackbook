   /*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.terminal;

import com.lariennalibrary.blackbook.BB;
//import static com.lariennalibrary.blackbook.BB.active_context;
import com.lariennalibrary.blackbook.ICommand;
//import com.lariennalibrary.blackbook.SCommandTreeNode;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;
//import sun.misc.BASE64Decoder;


/**
 * This is an interactive command line interface made with java swing to test
 * the command line system and play some games.
 * 
 * The structure of the class is a bit different as it tries to respect the 
 * NOOP paradigm. The class act both as a view and presenter.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class SwingTerminal 
{
   /**List of color constants organised in an hashmap*/
   public static final Map<String, Color> COLOR = new HashMap<> ();
      
   /**List of font types organised in an hashmap*/
   public static final Map<String, Integer> FONT_TYPE = new HashMap<> ();
   
   /**initialise the hashmap of constants*/
   static 
   {      
      COLOR.put ("Black", Color.BLACK);
      COLOR.put ("Blue", Color.BLUE);
      COLOR.put (    "Cyan", Color.CYAN);
      COLOR.put (    "Dark Gray", Color.DARK_GRAY);
      COLOR.put (    "Gray", Color.LIGHT_GRAY);
      COLOR.put (    "Green", Color.GREEN);
      COLOR.put (    "Magenta", Color.MAGENTA);
      COLOR.put (    "Orange", Color.ORANGE);
      COLOR.put (    "Pink", Color.PINK);
      COLOR.put (    "Red", Color.RED);
      COLOR.put (    "White", Color.WHITE);
      COLOR.put (    "Yellow", Color.YELLOW);       
      FONT_TYPE.put(      "Plain", Font.PLAIN);
      FONT_TYPE.put(       "Bold", Font.BOLD);
      FONT_TYPE.put(       "Italic", Font.ITALIC);           
   }          
   public static final int DEFAULT_FONT_SIZE = 16;
   public static final int DEFAULT_FONT_TYPE = Font.PLAIN;
   public static final Color DEFAULT_FG_COLOR = Color.WHITE;
   public static final Color DEFAULT_BG_COLOR = Color.BLACK;
   
   public static final String base64icon = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4wYdECkPueiYZAAAASNJREFUeNrtm1sOgyAQAKHxTnIgvJUHgkM19MvE0Kq8qzCb+KOhbGeniNkqjTFvMXC8xOABAAAAAAAAAAAAAAAAAABgzJhiB8zz/PO8lDJ6cudc9lg/rLX1DFjX9fYVjc0xGIBS6qvi+6N1nM3v55oN4AmVT815Cqm81vrw+tm1lMRKfp5SShhjyi6Cte1obVs2gDsb0AQABmAABmAABmAABjQxoAYcDChtwNMenKanVaz7NQADMAADMAADMAAD+jHg7oCGN4Du8OgAkn8Cfne2dX/wqDuMAbUNWJalyH5gb03K2KM8qgKoteL/804SDCClSldfeOvcbv38knMEW3j1wkTMnw1iwm9bt5onGgD7AAAAAAAAAAAAAAAAAAAAAADoMD4cZJO1yXyymAAAAABJRU5ErkJggg==";
   
   public static JFrame create ()
   {
      //Creating and defining the window.
      JFrame frm_window = new JFrame();
      
      frm_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      //Dimension screen_size = Toolkit.getDefaultToolkit().getScreenSize();
      //frm_window.setBounds( 0, 0, screen_size.width, screen_size.height);
      
      frm_window.setExtendedState(JFrame.MAXIMIZED_BOTH); 
      //frm_window.setUndecorated(true); //removes title and other
      
      frm_window.setLayout ( new BorderLayout( 5, 5 ) ); //nb of pixel for padding
      //frm_window.setLayout ( new GridLayout (3,1));
      frm_window.setTitle ( "Black Book Terminal [|]");
      frm_window.setIconImage(decodeToImage (base64icon));
      //frm_window.getContentPane().setBackground( Color.GRAY );
      
     
      /*The icon can be set this way, but it requires a file
      
      ImageIcon img = new ImageIcon(pathToFileOnDisk);
      myFrame.setIconImage(img.getImage());
      
      Look at the following links to convert an image as base 64
      
      http://electrocarta.blogspot.com/2017/05/how-to-convert-imageicon-to-base64.html
      https://javapointers.com/tutorial/java-convert-image-to-base64-string-and-base64-to-image/
      */
      
      
      //Creating a monospace font used by most widgets
      
      Font fnt_monospace = new Font("monospaced", DEFAULT_FONT_TYPE,
         DEFAULT_FONT_SIZE );
      
      //--- output area ---
            
      JTextArea txa_leftout = new JTextArea(25, 80); //nb of lines and columns
      txa_leftout.setEditable(false);
      //txa_leftout.setWrapStyleWord(false);
      //txa_leftout.setLineWrap(true);
      txa_leftout.setText( BB.left_out.toString() ); //initialise with out buffer
      txa_leftout.setFont( fnt_monospace );
      txa_leftout.setForeground( DEFAULT_FG_COLOR);
      txa_leftout.setBackground( DEFAULT_BG_COLOR);
      
      //--- Scroll Bars for the left output
      JScrollPane scr_leftout = new JScrollPane(txa_leftout);
                  
      //--- Right output buffer ---
      //could be done or if there are other ways to size this
      JTextArea txa_rightout = new JTextArea( 25, 80 );
      txa_rightout.setEditable(false);
      txa_rightout.setText(BB.right_out.toString());
      txa_rightout.setBackground(DEFAULT_BG_COLOR);
      txa_rightout.setForeground(DEFAULT_FG_COLOR);
      txa_rightout.setFont(fnt_monospace);
      
      //--- Scroll Bars for the right output
      JScrollPane scr_rightout = new JScrollPane(txa_rightout);
      
      //--- text input area ---
      
      JTextField txt_input = new JTextField();
      txt_input.setFont(fnt_monospace);
      txt_input.setForeground(DEFAULT_FG_COLOR);
      txt_input.setBackground(DEFAULT_BG_COLOR);
      txt_input.setCaretColor(DEFAULT_FG_COLOR);
      txt_input.setFocusTraversalKeysEnabled(false);
      
      //--- Command Prompt ---
      JLabel lbl_cmdprompt = new JLabel ("Input ->");
      lbl_cmdprompt.setFont(fnt_monospace);
      
      //--- command history area ---
      JTextArea txa_contexthelp = new JTextArea( 3, 100 ); //nb of lines and columns
      //NOTE: Size is determined by the anchor point for the BorderLayout or the
      //number of char of the area.
      txa_contexthelp.setEditable(false);
      txa_contexthelp.setWrapStyleWord(false);
      txa_contexthelp.setLineWrap(true);
      txa_contexthelp.setText ("");
      txa_contexthelp.setFont( fnt_monospace );
      txa_contexthelp.setForeground(DEFAULT_FG_COLOR);
      txa_contexthelp.setBackground(DEFAULT_BG_COLOR);   
      
      //--- Scroll Bars for the history ---
      //JScrollPane scr_history = new JScrollPane(txa_history);
      
      //--- Button Group to prevent multi push---
      ButtonGroup grp_font_size = new ButtonGroup();
      ButtonGroup grp_font_type = new ButtonGroup();
      ButtonGroup grp_color_fg = new ButtonGroup();
      ButtonGroup grp_color_bg = new ButtonGroup();
                  
      //--- Menu Bar creation ---
      JMenuBar mnb_terminal = new JMenuBar();
      JMenu mnu_app = new JMenu ("Application");
      JMenu mnu_font = new JMenu ("Font");
      JMenu mnu_color = new JMenu ("Color");
      JMenuItem mni_app_exit = new JMenuItem ("Exit");
      JMenu mnu_font_size = new JMenu ("Size");
      JMenu mnu_font_type = new JMenu ("Type");
      JMenu mnu_color_fg = new JMenu ("Foreground");
      JMenu mnu_color_bg = new JMenu ("Background");
      
            
      //Configure font size: Widget, listener, color, group, etc.
      FontSizeListener fs_listener = new FontSizeListener
        ( txa_leftout, txa_rightout, txa_contexthelp, txt_input, lbl_cmdprompt );
      for ( int i = 10 ; i <= 20; i = i+2)
      {
         JRadioButtonMenuItem rmi = new JRadioButtonMenuItem ( Integer.toString(i) );
         //rmi.setForeground(entry.getValue());
         //if ( Color.BLACK == entry.getValue()) rmi.setForeground (Color.WHITE );  
         //rmi.setBackground( Color.BLACK );
         Font tmpfont = new Font ( "monospaced", DEFAULT_FONT_TYPE, i);
         rmi.setFont(tmpfont);
         mnu_font_size.add (rmi);
         rmi.addActionListener(fs_listener);
         grp_font_size.add (rmi);
         if ( i == DEFAULT_FONT_SIZE ) rmi.setSelected(true);
      }
      
      //Configure font type: Widget, listener, color, group, etc.
      FontTypeListener ft_listener = new FontTypeListener
        ( txa_leftout, txa_rightout, txa_contexthelp, txt_input, lbl_cmdprompt );
      for ( Map.Entry<String, Integer> entry: FONT_TYPE.entrySet())
      {
         JRadioButtonMenuItem rmi = new JRadioButtonMenuItem ( entry.getKey());
         //rmi.setForeground(entry.getValue());
         //if ( Color.BLACK == entry.getValue()) rmi.setForeground (Color.WHITE );  
         //rmi.setBackground( Color.BLACK );
         //System.out.printf ("DEBUG: %s : %d",  entry.getKey(), entry.getValue());
         Font tmpfont = new Font ( "monospaced", entry.getValue(), DEFAULT_FONT_SIZE);
         rmi.setFont(tmpfont);
         mnu_font_type.add (rmi);
         rmi.addActionListener(ft_listener);
         grp_color_fg.add (rmi);
         if ( DEFAULT_FONT_TYPE == entry.getValue() ) rmi.setSelected(true);
      }
      
      
      //Configure foreground colors: Widget, listener, color, group, etc.
      ForegroundListener fg_listener = new ForegroundListener
        ( txa_leftout, txa_rightout, txa_contexthelp, txt_input );
      for ( Map.Entry<String, Color> entry: COLOR.entrySet())
      {
         JRadioButtonMenuItem rmi = new JRadioButtonMenuItem ( entry.getKey());
         rmi.setForeground(entry.getValue());
         //if ( Color.BLACK == entry.getValue()) rmi.setForeground (Color.WHITE );  
         rmi.setBackground( Color.BLACK );
         mnu_color_fg.add (rmi);
         rmi.addActionListener(fg_listener);
         grp_color_fg.add (rmi);
         if ( DEFAULT_FG_COLOR == entry.getValue() ) rmi.setSelected(true);
      }
      
      //Configure Background colors: Widget, listener, color, group, etc.
      BackgroundListener bg_listener = new BackgroundListener
              ( txa_leftout, txa_rightout, txa_contexthelp, txt_input );
      for ( Map.Entry<String, Color> entry: COLOR.entrySet())
      {
         JRadioButtonMenuItem rmi = new JRadioButtonMenuItem ( entry.getKey());
         rmi.setBackground(entry.getValue());
         rmi.setForeground( Color.WHITE );
         mnu_color_bg.add (rmi);
         rmi.addActionListener(bg_listener);
         grp_color_bg.add (rmi);
         if ( DEFAULT_BG_COLOR == entry.getValue() ) rmi.setSelected(true);
      }
      
      //--- Menu Bar composition ---
      mnu_app.add (mni_app_exit);
      mnu_font.add (mnu_font_size);
      mnu_font.add (mnu_font_type);
      mnu_color.add (mnu_color_fg);
      mnu_color.add (mnu_color_bg);
      mnb_terminal.add(mnu_app);
      mnb_terminal.add(mnu_font);
      mnb_terminal.add(mnu_color);
      
      
      
      //defining pannels
      JPanel pan_top = new JPanel ( new BorderLayout (5, 5));
      //pan_top.setBackground(Color.GRAY);
      JPanel pan_bottom = new JPanel ( new BorderLayout (5, 5));
      //pan_top.setBackground(Color.GRAY);
      
      //--- Adding terminal output scrollpane to split pane ---
      JSplitPane spl_output = new JSplitPane ( JSplitPane.HORIZONTAL_SPLIT,
         scr_leftout, scr_rightout );
      spl_output.setOneTouchExpandable(true); //Not sure what it does
      //spl_output.setDividerLocation(0.5);
      spl_output.setResizeWeight(0.5);
      
      
      
      //--- adding widgets to pannels and window ---
      pan_top.add (spl_output, BorderLayout.CENTER );
      pan_top.add (txa_contexthelp, BorderLayout.SOUTH );
      pan_bottom.add (lbl_cmdprompt, BorderLayout.WEST);
      pan_bottom.add (txt_input, BorderLayout.CENTER );
      //pan_bottom.add (scr_history, BorderLayout.SOUTH );

      frm_window.add ( pan_top, BorderLayout.CENTER );
      frm_window.add ( pan_bottom, BorderLayout.SOUTH ); 
      //pan_input.add (lbl_cmdprompt, BorderLayout.WEST );
      //pan_input.add (txt_input, BorderLayout.CENTER );
            
      //frm_window.add ( spl_output );
      //frm_window.add ( txa_contexthelp );
      //frm_window.add ( pan_input );
      
      
      
      //--- Add the menu to the window
      frm_window.setJMenuBar ( mnb_terminal );
      
      frm_window.setVisible (true);
      
      //----- Adding Listener not added so far-----
      
      mni_app_exit.addActionListener(new ExitAppListener());
      txt_input.addActionListener(new InputCommandListener 
        ( txa_leftout, txa_rightout ));
      txt_input.addKeyListener ( new ShowHistoryHelpListener( txa_contexthelp));
      
      txt_input.requestFocus();        
      
      txa_contexthelp.setText( BB.get_context_sensitive_help ( "") );
      
      return frm_window;
   }
   
   
   //TODO SAVE/LOAD CONFIGURATION seems a bit more complicated, need to isolate
   // the code in the listener in a separate function to be able to call
   // them from the load function
   
   /*BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader(
					"/Users/pankaj/Downloads/myfile.txt"));
			String line = reader.readLine();
			while (line != null) {
				System.out.println(line);
				// read next line
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}*/
   
   //----------------------------------------------------------------
   //--------------  Listeners --------------------------------------
   //----------------------------------------------------------------
   
   /**
    * Listener for exiting the app
    */
   private static class ExitAppListener implements ActionListener
   {
      @Override
      public void actionPerformed(ActionEvent e)
      {
         System.exit(0);
      }
   }
   
   /**
    * Listener for changing the foreground color
    * 
    * design note: In order to set the caret color, JTextComponent are used
    */
   private static class ForegroundListener implements ActionListener
   {
      private final ArrayList<JTextComponent> _list;
      
      /**
       * Requires a list of components to modify with the selected color
       * when the listener will be fired.
       * 
       * @param list of components than needs to be changed of colors
       */
      public ForegroundListener ( JTextComponent ...list)
      {
         _list = new ArrayList<>();
         _list.addAll(Arrays.asList(list));            
      }
      
      /**
       * It reuse the color in the menu widget to display the right color. It
       * prevents the need to pass the value in parameter
       * @param e 
       */
      @Override
      public void actionPerformed(ActionEvent e)
      {
      //   System.out.printf ("DEBUG: Foreground %s\n", e.getActionCommand());
         JComponent src = (JComponent) e.getSource();
         for ( JTextComponent cmp: _list )
         {
            cmp.setForeground ( src.getForeground() );
            
            /*if ( cmp.getClass().getName().equals( "javax.swing.JTextField"))
            {  
               
            }*/
            //System.out.println ( cmp.getClass().getName());
            //check if can find text input, else force parameter
            cmp.setCaretColor(src.getForeground());
         }         
      }
      
   }
           
   /**
    * Listener for changing the background color
    */
   private static class BackgroundListener implements ActionListener
   {  private final ArrayList<JComponent> _list;
      
      /**
       * Requires a list of components to modify with the selected color
       * when the listener will be fired.
       * 
       * @param list of components than needs to be changed of colors
       */
      public BackgroundListener ( JComponent ...list)
      {
         _list = new ArrayList<>();
         _list.addAll(Arrays.asList(list));            
      }
      
      /**
       * It reuse the color in the menu widget to display the right color. It
       * prevents the need to pass the value in parameter
       * @param e 
       */
      @Override
      public void actionPerformed(ActionEvent e)
      {
         //System.out.printf ("DEBUG: Background %s\n", e.getActionCommand());
         JComponent src = (JComponent) e.getSource();
         for ( JComponent cmp: _list ) cmp.setBackground ( src.getBackground() );         
      }
      
   }
    
   /**
    * Listener for changing the font size
    */
   private static class FontSizeListener implements ActionListener
   {  private final ArrayList<JComponent> _list;
      
      /**
       * Requires a list of components to modify with the selected font size
       * when the listener will be fired.
       * 
       * @param list of components than needs to be changed of colors
       */
      public FontSizeListener ( JComponent ...list)
      {
         _list = new ArrayList<>();
         _list.addAll(Arrays.asList(list));            
      }
      
      /**
       * It reuse the size of the font in the menu widget to display the right 
       * size. It prevents the need to pass the value in parameter
       * 
       * @param e 
       */
      @Override
      public void actionPerformed(ActionEvent e)
      {         
         JComponent src = (JComponent) e.getSource();
         
         for ( JComponent cmp: _list )
         {
            Font oldfont = cmp.getFont();            
            cmp.setFont ( new Font ( oldfont.getName(), oldfont.getStyle(),
                          src.getFont().getSize()));
         }         
      }
      
   }
   
   /**
    * Listener for changing the font type
    */
   private static class FontTypeListener implements ActionListener
   {  private final ArrayList<JComponent> _list;
      
      /**
       * Requires a list of components to modify with the selected font size
       * when the listener will be fired.
       * 
       * @param list of components than needs to be changed of colors
       */
      public FontTypeListener ( JComponent ...list)
      {
         _list = new ArrayList<>();
         _list.addAll(Arrays.asList(list));            
      }
      
      /**
       * It reuse the size of the font in the menu widget to display the right 
       * size. It prevents the need to pass the value in parameter
       * 
       * @param e 
       */
      @Override
      public void actionPerformed(ActionEvent e)
      {         
         JComponent src = (JComponent) e.getSource();
         
         for ( JComponent cmp: _list )
         {
            Font oldfont = cmp.getFont();            
            cmp.setFont ( new Font ( oldfont.getName(), 
                          src.getFont().getStyle(), oldfont.getSize()));
         }         
      }
      
   }
   
   /**
    * This is the action listener used to interpret inputed command and offer
    * interactive keyboard features like arrows for history recall.
    * 
    * design notes: ActionPerformed is triggered on the press of the enter key
    * so there is no need to check for it
    */
   private static class InputCommandListener implements ActionListener
   {
      private final JTextArea _txa_leftout;
      private final JTextArea _txa_rightout;
      //private final JTextArea _txa_help;
      
      
      public InputCommandListener (JTextArea leftout, JTextArea rightout)
      {
         _txa_leftout = leftout;
         _txa_rightout = rightout;
         //_txa_help = help;
      }
      
      @Override
      public void actionPerformed(ActionEvent e)
      {
         JTextField fld = (JTextField)e.getSource();
         //BB.out.println( fld.getText());
         BB.parse_input(fld.getText());         
         //_txa_rightout.setText ( _txa_rightout.getText() + "\n" + fld.getText());
         fld.setText(BB.prefix);
         
         _txa_leftout.setText( BB.left_out.toString());
         _txa_rightout.setText( BB.right_out.toString());
         
         //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
      }
      
   }
   
   
   
   /**
    * Listener that take cares of trapping the arrows for history control. It
    * is also used or search as you type features
    * 
    */
   
   private static class ShowHistoryHelpListener implements KeyListener
   {
      private final JTextComponent _helpout;
      //private static int debug_count = 0;
      
      /** Must specity a widget to output the context sensitive help
       */      
      public ShowHistoryHelpListener ( JTextComponent help_output )
      {
         _helpout = help_output;
      }
      
      @Override
      public void keyTyped(KeyEvent e)           
      { 
         
      }
      
      @Override
      public void keyReleased(KeyEvent e) 
      {
         if ( e.isActionKey() == false )
         {
            JTextField fld = (JTextField)e.getSource();
            //TODO BUG EVENT FIRE maybe no need to filter action key, 
            //must get called anyway by keypressed.
            _helpout.setText( BB.get_context_sensitive_help ( fld.getText()) );
            //System.out.printf ("%d: DEBUG: This is not an action key\n", debug_count);
            //debug_count++;
         }
      }

      @Override
      public void keyPressed(KeyEvent e)
      {
         JTextField fld = (JTextField)e.getSource();
         String tmpstr = null;
         switch(e.getKeyCode()) 
         {  case KeyEvent.VK_UP:                
               tmpstr = BB.in.get_previous();               
            break;
            case KeyEvent.VK_DOWN:
               tmpstr = BB.in.get_next();
            break;
            case KeyEvent.VK_TAB:
               //System.out.println ("I trapped the tab completion");
               tmpstr = BB.get_tab_completion(fld.getText());               
            break;   
         }     
         
         if ( tmpstr != null)
         {
            _helpout.setText( BB.get_context_sensitive_help ( tmpstr ) );
            fld.setText ( tmpstr );
         }
      }
   }  
   
   /**
    * Convert a base 64 image into a BufferedImage
    * 
    * Source code taken and adapted fromfrom:
    * https://javapointers.com/tutorial/java-convert-image-to-base64-string-and-base64-to-image/
    * 
    * @param imageString Base64 encoded string
    * @return 
    */
   
   private static BufferedImage decodeToImage(String imageString) {
 
        BufferedImage image = null;
        byte[] imageByte;
        try {
            Decoder decoder = Base64.getDecoder();
            imageByte = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
   
}



/*
//Code from the old project, and example of terminal

 public void start ( )
    {
        String commandline = "";
        String[] cmdltokens = null;
        keyboard = new BufferedReader ( new InputStreamReader ( System.in ));
        
        while ( commandline.equals("quit") == false)
        {
            try
            {
                System.out.printf ("> ");
                commandline = keyboard.readLine();
            }
            catch (IOException ex)
            {
                System.out.println ("Exception encountered, ignoring command");
                ex.printStackTrace();
                
                commandline = "";
            }
            
            cmdltokens = commandline.split (" ");
            System.out.println ("You typed the following tokens ");
            
            for ( String token: cmdltokens )
            {
                System.out.println ( token );
            }    
        }    
        
    }
*/