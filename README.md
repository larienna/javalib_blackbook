# Black Book Library `` [|] ``
Copyright Eric Pietrocupo
Apache 2 License

Black Book is used to design terminal based turn based strategy video games. It could be used for various purpose:

* Create new video games
* Use as sidekicks for board games
* Prototype new video games or board games

This library is the 1st phase for the implementation of turn based strategy video games. It focus on the core mechanics of the game rather than the video interface. During phase 2 another library will be build to use games made with Black Book and move them to graphic environment.

The root contains the code of the library, but there are many subprojects:

* Black Book Terminal: This is a Java Swing terminal for development purpose on desktop. Your game project should use this terminal for interactive play. But it will be stripped away when the game is ported as a graphic game.
* Black Book Demo: This project is the equivalent of the game you want to build. You will create a new project for each game built. (Note: It's possible that in the future, it get split into a launcher and a rulebox for easier modularity)

in the "example" folder there will be many project which are each a sample game that use Black Book as a demonstration.

## Road Map

This is an approximative road map according to the features that could be required by a game. They are in order of priority, but I could change the order any time.

### Game features to implement

* __Simple Data__: Add suport for JSON and make the game data contained in a single POJO. Useful for simple games with limited amount of data to record.
* __Complex Data__: Support relational database for more complex data structre. Relational database would allow by itself certain structures like single hop graphs, trees, etc. Most modern games will have complex data: Card games with multiple information, Combat resolution tables, Resource management, etc
* __Grids__: Allow games with grids to be supported. Grids includes: square, hex and triangular grids. Games that modifies the content of a grid could be implemented in a second phase. Most ancient games use grids, so they are not to be ignored.
* __Text Abilities__: Support of lua scripts to be able to put special abilities on each game element. For example, text abilities on cards or anything game element that has unique rules attached to it.

### Excluded features

There will be some overlap between my game engine and board games, but certain board games mechanics will not be implemented with my engine. Here is a couple of concepts that should never make it for various reasons. I can still change my mind.

* __Multihop Graphs__: Games with graph that has movement across multiple nodes. In that case relational database are not efficient enough for multiple hop calculations, the more hops needed the slower it will be. For example Pax Romana(2006).
* __Undefined grid size__: Games like Carcassonne where tiles are put on a grid but there is no limit to the expansion of the tiles.
* __Dexterity games__: Games that requires a dexterity element: shooting, flipping, throwing, etc. Ex: Tumbling dice, catacombs(2010)
* __3D landscape__: Games like Hero Scape will not be possible. It will be possible to have a 2D grid with a height value like on Battletech maps, but not having a 3d landscape with overlapping areas.
* __Analog games__: Games like warhammer that requires measuring distance will not be implemented. I also include here hacker and illuminati where card placement cannot overlap each other.
* __Certain modular boards__: Some modular game are just variable grids, but other games could be much more complex to implement. My game Fallen Kingdoms where each tile has a graph that can connect to other tiles could be problematic and might require custom implementation. The requiment of multihoping will also impact if it can be implemented.
* __Acting and performance__: That goes by itself, you cannot really perform with a video game. You'll have to wait until androids with emotions exists.
* __Negotiation__: Games that requires a lot of back and forth between players. Trading in settlers of catan is simple enough to implement, but "I'am the boss" is not.
* __Realtime mechanism__: Games that has timers will not be supported.


## Usage

The library has very little features so far, so I do not recommend building anything yet at this point. I'll be making sample games in the next milestone, which will make them interesting study cases.

A game will be composed of 2 parts:

* Data: That will be contained into a relational database
* Code: That will be contain in a class and a series of command classes called rule box

The user interact with the game by using commands defined by the game or by the black book system. Those commands will transform the data stored in the database which will act as a save game.

### Relational Database

The relational database system does not work yet, so more information will be available in the next release.

There is some possibilities of using somehting more simple than a relational database for games that does not requires a lot of data. For example, using a POJO and converting it with JSON.

It is possible not to use any of the data structure above, you'll just have no way to save and load games or you'll have to supply your own code.

### Rulebox Class

Here is some sample code to get you started. First you need to make a class implement the IRuleBox interface like this:

```java
public class RuleBox implements IRuleBox
{
   private final Map<String, ICommand> _commands;   
    
   public RuleBox ()
   {
      _commands = new HashMap<>();

      _commands.put("example", new Example());
   }
   
   @Override
   public String gameinfo()
   {  
      return "Larienna's Library presents\nExample Game\nVersion 0.1 (2019)\nProgrammed by Eric Pietrocupo (larienna@mail.com)\n";
   }

   @Override
   public void application_startup()
   {
      
   }

   @Override
   public void game_start(boolean newgame)
   {
      
   }

   @Override
   public void game_end(boolean gameover)
   {
      
   }

   @Override
   public Map<String, ICommand> get_commands(String context)
   {
      return _commands;
   }        
}
```

In the code above you create an internal list of commands and return it when Black Book will ask for it. Commands are defined in another block of code, they must each implement the ICommand interface. They can be located as nested classes inside the rulebox or any other class. Here is an example of command, in my case, I made them static inner class:

```java

    public static class CmdBonjour implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("Bonjour from command");
         return 0;
      }

      @Override
      public String help()
      {  return ("Show the word 'Bonjour' in the console command");  
      }

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
```

The parameter system does not work so far, eventually, it will be possible to define a list of parameters with expected types, and black book will validate those parameters for you to make sure you don't need to do it your self. At this point, you should have a working rulebox that contains all the mechanics of your game. The commands can be spread among different classes to make sure it's not too crowded and easier to navigate when you are looking for a piece of code.

### Launcher

Finally, you find a way to launch your rulebox game. There will be multiple kind of launchers, but for now there is only one: SwingTerminalLauncher. The idea is to create the launcher you need and pass in parameter your game's rulebox so that it can run the game for your. SwingTerminalLauncher will create a terminal using java swing widget system, you will then be able to play your game from the command line. Here is how you create a launcher and attach your game:


```java

public static void main(String[] args) 
{
	SwingTerminalLauncher launcher = new SwingTerminalLauncher();
    launcher.start ( new DemoGame );
}

```

## Download example binairies

Examples have been precompiled, and the binairies are available for download if you want to test them without downloading and building the code. They are distributed as zip packages. There is a run script (.bat for windows user) in the "bin" subfolder that you can run to start the game. Here are the games available:

[Poker Dice](./examples/distribution/poker_dice.zip)
