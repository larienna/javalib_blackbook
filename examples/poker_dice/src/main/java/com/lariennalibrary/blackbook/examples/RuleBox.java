package com.lariennalibrary.blackbook.examples;

import com.lariennalibrary.blackbook.ICommand;
import com.lariennalibrary.blackbook.IRuleBox;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of the poker dice game. This example's feature are:
 * 
 * - Minimalilst requirements to make a game
 * - Rulebox made by hand
 * - Command library made by hand
 * - Data is contained in a single Structure class
 * - There is no savegame possibilities
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class RuleBox implements IRuleBox
{
   /**Total number of dice in the set*/
   public static final int NB_DICE = 5;
   /**Default number of rerolls*/
   public static final int NB_REROLL = 1;
   
   private final Map<String, ICommand> _newroll_commands;
   private final Map<String, ICommand> _reroll_commands;
   private final Map<String, ICommand> _init_commands;
   
   
   /**Game data accesses statically from them methods*/
   public static SGameData data;
    
   public RuleBox ()
   {
      _newroll_commands = new HashMap<>();
      _reroll_commands = new HashMap<>();
      _init_commands = new HashMap<>();
      data = new SGameData();

      //building the newroll context
      _newroll_commands.put("show payout", new CommandLibrary.ShowPayout());
      _newroll_commands.put("show player", new CommandLibrary.ShowPlayer());
      _newroll_commands.put("bid", new CommandLibrary.ChangeBid());
      _newroll_commands.put("roll", new CommandLibrary.RollDice());
      
      //building the reroll context
      _reroll_commands.put("show payout", new CommandLibrary.ShowPayout());
      _reroll_commands.put("show player", new CommandLibrary.ShowPlayer());
      _reroll_commands.put("keep", new CommandLibrary.KeepDice());
      
      //building the init context
      _init_commands.put("start", new CommandLibrary.StartGame());
      
      
   }
   
   @Override
   public String gameinfo()
   {  
      return "Larienna's Library presents\nPoker Dice\nVersion 0.1 (2019)\nProgrammed by Eric Pietrocupo (larienna@mail.com)\n";
   }

   @Override
   public void application_startup()
   {
      
   }

   @Override
   public void game_start(boolean newgame)
   {
      
   }

   @Override
   public void game_end(boolean gameover)
   {
      
   }

   @Override
   public Map<String, ICommand> get_commands(String context)
   {
      switch ( context )
      {
         case "NEWROLL":
            return _newroll_commands;
         case "REROLL":
            return _reroll_commands;
         case "INIT":
            return _init_commands;
      }    
      
      return null;
   }
   
  
    
   
    
    
    
    
    
            
}

