package com.lariennalibrary.blackbook.examples;

import com.lariennalibrary.blackbook.ICommand;
import com.lariennalibrary.blackbook.IRuleBox;
import java.util.HashMap;
import java.util.Map;

public class RuleBox implements IRuleBox
{
   private final Map<String, ICommand> _commands;
   
    
   public RuleBox ()
   {
      _commands = new HashMap<>();

      _commands.put("example", new CommandLibrary.Example());
   }
   
   @Override
   public String gameinfo()
   {
      
      
      return "Larienna's Library presents\nPoker Dice\nVersion 0.1 (2019)\nProgrammed by Eric Pietrocupo (larienna@mail.com)\n";
   }

   @Override
   public void application_startup()
   {
      
   }

   @Override
   public void game_start(boolean newgame)
   {
      
   }

   @Override
   public void game_end(boolean gameover)
   {
      
   }

   @Override
   public Map<String, ICommand> get_commands(String context)
   {
      return _commands;
   }
   
  
    
   
    
    
    
    
    
            
}

