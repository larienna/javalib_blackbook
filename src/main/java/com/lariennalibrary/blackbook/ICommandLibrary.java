/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

import java.util.Map;

/**
 * This is a simple interface used for classes that has commands to offer to
 * the black book engine. Black Book will be able to hold a list of command
 * library to build his master list of commands when the context is changed.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public interface ICommandLibrary 
{
   /**
    * Return a list of commands for the game
    * @param context this is the name of the context you want to get the commands
    * @return a list of commands to be executed within this context.
    */
   public Map<String, ICommand> get_commands ( String context );

}
