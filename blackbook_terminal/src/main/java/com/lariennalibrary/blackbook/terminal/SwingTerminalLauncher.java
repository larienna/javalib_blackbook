/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.terminal;

import com.lariennalibrary.blackbook.BB;
import com.lariennalibrary.blackbook.ILauncher;
import com.lariennalibrary.blackbook.IRuleBox;
import javax.swing.JFrame;

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class SwingTerminalLauncher implements ILauncher
{
   @Override
   public void start(IRuleBox rulebox)
   {
      BB.init( rulebox );
      
      //building the test command tree
      rulebox.application_startup();
      
      JFrame terminal = SwingTerminal.create();      
   }

   /*@Override
   public void exit()
   {
      System.exit(0);
   }*/

}
