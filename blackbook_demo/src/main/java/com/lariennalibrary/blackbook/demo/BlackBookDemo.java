package com.lariennalibrary.blackbook.demo;

import com.lariennalibrary.blackbook.BB;
import com.lariennalibrary.blackbook.ICommand;
import com.lariennalibrary.blackbook.IRuleBox;
//import com.lariennalibrary.blackbook.SContext;
//import com.lariennalibrary.blackbook.terminal.SwingTerminal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BlackBookDemo
{
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
    {  System.out.println("Black Book Demo\n");
       //DemoTest.terminal(new DemoGame());
       //DemoTest.command_tree(new DemoGame());
       //DemoTest.interpreter(new DemoGame());
       //DemoTest.buffers(new DemoGame());
       //DemoTest.inbuffer(new DemoGame());
       //DemoTest.speed_test(new DemoGame());
       DemoTest.launcher( new DemoGame());
              
    }
    
   /**
    * A simple demonstration game for testing purpose.
    */ 
    
   public static class DemoGame implements IRuleBox
   {
      private final Map<String, ICommand> _cmdall;
      
      public DemoGame ()
      {
         _cmdall = new HashMap<>();
         
         _cmdall.put("show bonjour", new CmdBonjour());
         _cmdall.put("show hello", new CmdHello());
         _cmdall.put("compute add", new CmdAdd());
         _cmdall.put("subranch show bonjour", new CmdBonjour());
         _cmdall.put("subranch show hello", new CmdHello());
         _cmdall.put("subranch compute add", new CmdAdd());
      }
      
      @Override
      public String gameinfo()
      {
         return "Larienna's Library presents\nDemonstration game\nVersion 2.0 (2019)\nDesigned by Eric Pietrocupo (larienna@mail.com)";      
      }

      @Override
      public void application_startup()
      {
         //System.out.println ("DEBUG: Passed the application_startup method");
         
         //creating a command tree structure and setting GAME as the current context
         /*SContext gamectx = new SContext ("GAME");
         BB.context_list.add (gamectx);

         gamectx.commands.add ("show");
         gamectx.commands.add ("compute");
         gamectx.commands.add ("subranch");    

         gamectx.commands.child("show").add( new CmdBonjour());      
         gamectx.commands.child("show").add( new CmdHello());      
         gamectx.commands.child("compute").add( new CmdAdd());
         gamectx.commands.child("subranch").add (gamectx.commands.child("show") );

         BB.active_context = gamectx;    */     
      }

      @Override
      public void game_start(boolean newgame)
      {
         System.out.println ("DEBUG: Passed the game_start method");         
      }

      @Override
      public void game_end(boolean gameover)
      {
         System.out.println ("DEBUG: Passed the game_end method");         
      }

      @Override
      public Map<String, ICommand> get_commands(String context)
      {
         return _cmdall;
      }
   }
    
   
   
    
    //----- new test command -----
    public static class CmdBonjour implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("Bonjour from command");
         return 0;
      }

      @Override
      public String help()
      {  return ("Show the word 'Bonjour' in the console command");  
      }

      /*@Override
      public String name()
      {  return ("show bonjour");
      }*/

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
    
    
     public static class CmdShow implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("Showing stuff");
         return 0;
      }

      @Override
      public String help()
      {  return ("This command is used to show stuff");  
      }

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
    
    
    public static class CmdHello implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {
         BB.out.println ("Hello from command");
         return 0;
      }

      @Override
      public String help()
      {
         return ("Shows the word 'Hello' on the terminal");
      }

      /*@Override
      public String name()
      {
         return ("show hello");
      }*/

      @Override
      public ArrayList<String> param_name()
      {
         return null;
      }
       
    }
  
    //----- new test command -----
    public static class CmdAdd implements ICommand
    {
       private final ArrayList<String> _param;
      public CmdAdd () 
      {
         _param = new ArrayList<String>();
         _param.add("Value 1");
         _param.add("Value 2");
         
      }
       
      @Override
      public int execute(ArrayList<String> param)
      {
         BB.out.println ("2 + 2 equals 4");
         return 0;
      }

      @Override
      public String help()
      {
         return ("Adds 2 number together passed in parameter");
      }

      /*@Override
      public String name()
      {
         return "compute add";
      }*/

      @Override
      public ArrayList<String> param_name()
      {
         return _param;
      }
       
    }
            
}

