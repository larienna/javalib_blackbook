/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook.examples;

import com.lariennalibrary.blackbook.BB;

/**
 * Contains the payout multiplier and description for the various hands that
 * can be rolled.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public enum EPayout 
{
   //odds calculated according to a single reroll, not considering face keeping 
   //so it's not entirely accurate, but it's to give an approximate fair payout
   //progression
   FIVE_OFA_KIND  ( "Five of a kind",625.0f  ), //  0.08% +  0.08% =  0.16%
   FOUR_OFA_KIND  ( "Four of a kind", 26.0f  ), //  1.93% +  1.89% =  3.82%
   STRAIGHT       ( "Straight",       16.5f  ), //  3.09% +  2.99% =  6.08%
   FULL_HOUSE     ( "Full House",     13.25f ), //  3.86% +  3.71% =  7.47%
   THREE_OFA_KIND ( "Three of a kind", 3.5f  ), // 15.43% + 13.04% = 28.47%
   TWO_PAIR       ( "Two Pairs",       2.5f  ), // 23.15% + 17.79% = 40.91%
   ONE_PAIR       ( "One Pair",        1.25f ), // 46.30% + 24.86% = 71.16%
   BUST           ( "Bust",            0.0f  ); 
   
   
   public final String name;
   public final float  multiplier;
   
   /**Contains the number of duplicate of that specific face in the hand
    * which means that if there is a 3 of a kind, it should contain something
    * like 3 3 3 1 1
    */
   private static int [] duplicate_face;
   /**Contains the number of times the duplicate number appear, so if the player
    * has a full house, it should contain the values 0 2 3 0 0. Don't forget 
    * that indexes goes from 0 to 4, not from 1 to 5.
    */
   private static int [] duplicate_count;

   EPayout ( String name, float multiplier )
   {
      this.name = name;
      this.multiplier = multiplier;
   }
   
   /**
    * Will analyse the dices in order to find poker hands. THe dice value must 
    * be serted in alphabetical order first. The payout as been assumed to be
    * for having a set of 5 dice.
    * 
    * @param dice list of sorted dice
    * @return and enum of the matching payout.
    */
   
   public static EPayout analyse_hand ( int []dice )
   {
      int face_to_search;
      int face_count;
      
      //lazy initialisation, avoid creation and destruction of new array at each call
      if ( duplicate_face == null ) duplicate_face = new int [RuleBox.NB_DICE];
      if ( duplicate_count == null ) duplicate_count = new int [RuleBox.NB_DICE];
      
      //reset values
      for ( int i = 0 ; i < duplicate_count.length ; i++) duplicate_count [ i ] = 0;
      
      //first pass: counting dupplicate faces and saving it for each die
      for ( int i = 0 ; i < RuleBox.NB_DICE ; i++)
      {
         face_to_search = RuleBox.data.dice [ i ];
         face_count = 0;
         for ( int j = 0 ; j < RuleBox.NB_DICE; j++)
         {
            if ( RuleBox.data.dice [ j ] == face_to_search ) face_count++;            
         }
         duplicate_face [ i ] = face_count;
      }
      
      //DEBUG
      /*BB.out.printf ("DEBUG: duplicates %d-%d-%d-%d-%d\n", 
              duplicate_face [0], duplicate_face [1], duplicate_face [2],
              duplicate_face [3], duplicate_face [4] );*/
      
      //second pass: count the number of duplicate numbers to establish nb of same face 
      for ( int i = 0 ; i < RuleBox.NB_DICE; i++ )
      {         
         duplicate_count [duplicate_face [i] - 1]++;
      }
      
      //DEBUG
      /*BB.out.printf ("DEBUG: count %d-%d-%d-%d-%d\n", 
              duplicate_count [0], duplicate_count [1], duplicate_count [2],
              duplicate_count [3], duplicate_count [4] );*/
      
      
      if ( duplicate_count[4] == 5 ) return EPayout.FIVE_OFA_KIND;
      
      if ( duplicate_count[3] == 4 ) return EPayout.FOUR_OFA_KIND;
      
      if ( duplicate_count[2] == 3 ) 
      {
         if ( duplicate_count[1] == 2) return EPayout.FULL_HOUSE;
         else return EPayout.THREE_OFA_KIND;
      }           
      
      if ( duplicate_count[1] == 4 ) return EPayout.TWO_PAIR;
      
      if ( duplicate_count[1] == 2 ) return EPayout.ONE_PAIR;      
      
      // check for straight, there is only 2 possible combinations
      if ( dice [ 4 ] == dice [3] + 1 &&
           dice [ 3 ] == dice [2] + 1 &&   
           dice [ 2 ] == dice [1] + 1 &&   
           dice [ 1 ] == dice [0] + 1 ) return EPayout.STRAIGHT;
      
      return EPayout.BUST; 
   }
}
