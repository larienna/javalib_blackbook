/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

/**
 * This is the input buffer to be used by the terminal. It is basically used
 * to keep an history of the commands typed. It is a rotating buffer, you set
 * the number of strings you want at creation, and that his the amount that is
 * kept in history.
 * 
 * In order to remember where the use is located in the history, a read index 
 * will be used to remember the last read command. But the read index will be
 * reset after each insertion. This simulates perfectly a command line history.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class TerminalInBuffer 
{
   /**Rotating buffer of string pointers*/
   private final String[] _buffer;
   /**Insertion point of the new string. Should point on null or next str to remove*/
   private int _insert_index;
   /**Last position that was read. Reset after each insertion.*/
   private int _read_index;
   
   /**
    * Creates a new input buffer with desired size
    * @param capacity the number of lines desired
    */
   
   public TerminalInBuffer ( int capacity )
   {
      _buffer = new String[capacity];
      _insert_index = 0;
      _read_index = 0;
   }
   
   /**
    * This method add a new string to the buffer where cursor is located
    * @param str string to be inserted in the buffer
    */
   
   public void put ( String str )
   {
      _buffer [_insert_index] = str;
      _insert_index++;
      _insert_index = _insert_index % _buffer.length;
      _read_index = _insert_index;        
   }
   
   /**
    * This method allow the recovery of the string in the buffer. If an offset of
    * 0 is placed, it means that the string atinsertion positioned is return. If
    * you want to get strings from the past, you need to increase the offset. 
    * 
    * Giving an ofset larger than the capacity will simply make the pointer 
    * rotate around. Therefore, the same string will be returned again.
    * 
    * This method does not modify the read index, therefore it will not remember
    * reading a previous command
    * 
    * @param offset to be substracted from the index
    * @return the corresponding string, or null if there is no string
    */
   
   public String get ( int offset )
   {
      return _buffer[ (_insert_index - offset) % _buffer.length];
   }
   
   /**
    * Return the previous string in the buffer and move the read index to that
    * new position. The index will only move if a non-null string has been found
    * 
    * @return previous string in the buffer, or null if there is nothing.
    */
   public String get_previous ()
   {
      int tmpindex = Math.floorMod (_read_index -1 , _buffer.length);
      //DEBUG
      //System.out.printf ("\nThe index is %d", tmpindex);
      //System.out.printf ("\nThe read index was %d", _read_index );
      //System.out.printf ("\n-1 modulo 50 equals %d", Math.floorMod (-1 , 50 ));
      //System.out.printf ("\nindex modulo length equals %d", Math.floorMod (_read_index -1 , _buffer.length) );
      if ( _buffer[tmpindex] != null)
      {
         _read_index = tmpindex;
         return _buffer[_read_index];
      }
      else return null;
      
   }
   
   /**
    * Return the next string in the buffer if not null. the index only moves if
    * the string is not null. The method also prevent exceeding the current 
    * insertion index.
    * @return 
    */
   public String get_next()
   {
      if ( _read_index != _insert_index )
      {
         int tmpindex = (_read_index + 1 ) % _buffer.length;
         if ( _buffer[tmpindex] != null && tmpindex != _insert_index)
         {
            _read_index = tmpindex;
            return _buffer[_read_index];
         }      
      }
      return null;
   }
}
