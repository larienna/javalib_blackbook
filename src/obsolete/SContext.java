/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

/**
 * This is a simple data structure that could have been implemented as a map 
 * because it currently hold only 2 value. But I estimate that new information
 * could eventually be available. The members are final, the content is only 
 * set at it's creation. All context has a valid instantiated Tree node of 
 * commmands.
 * 
 * Design notes: I am currently sequential reading to find a specific context. 
 * Still making a seperate class makes it easier to set a reference on the 
 * current context and get it's name for example.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class SContext 
{
   public final String name;
   public final SCommandTreeNode commands;
   
   public SContext (String name)
   {
      this.name = name;
      this.commands = new SCommandTreeNode( "." );
      //this is the root of the context identified with a dot.
   }
   
}
