package com.lariennalibrary.blackbook.examples;

import com.lariennalibrary.blackbook.BB;
import com.lariennalibrary.blackbook.ICommand;
import com.lariennalibrary.blackbook.IRuleBox;
//import com.lariennalibrary.blackbook.SContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RuleBox implements IRuleBox
{
   
   private final Map<String, ICommand> _cmdingame;
      
   public RuleBox ()
   {
      //TODO COMMANDS put real commands here, there is 2 context so far look 
      //at design notes
      _cmdingame = new HashMap<>();

      _cmdingame.put("show bonjour", new CmdBonjour());
      _cmdingame.put("show hello", new CmdHello());
      _cmdingame.put("compute add", new CmdAdd());
      _cmdingame.put("subranch show bonjour", new CmdBonjour());
      _cmdingame.put("subranch show hello", new CmdHello());
      _cmdingame.put("subranch compute add", new CmdAdd());
   }
   
   /**
    * A simple demonstration game for testing purpose.
    */ 
    
   @Override
   public String gameinfo()
   {
      return "Larienna's Library presents\nStock Ticker\nVersion 0.1 (2019)\nProgrammed by Eric Pietrocupo (larienna@mail.com)\nDesign based on the classic Stock Ticker game";      
   }

   @Override
   public void application_startup()
   {
      
   }

   @Override
   public void game_start(boolean newgame)
   {
      System.out.println ("DEBUG: Passed the game_start method");         
   }

   @Override
   public void game_end(boolean gameover)
   {
      System.out.println ("DEBUG: Passed the game_end method");         
   }

   @Override
   public Map<String, ICommand> get_commands(String context)
   {
      return _cmdingame;
   }
    
   
    
    //----- new test command -----
    public static class CmdBonjour implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {  BB.out.println ("Bonjour from command");
         return 0;
      }

      @Override
      public String help()
      {  return ("Show the word 'Bonjour' in the console command");  
      }

      

      @Override
      public ArrayList<String> param_name()
      {  return null;
      }
       
    }
    
    //----- new test command -----
    public static class CmdHello implements ICommand
    {
      @Override
      public int execute(ArrayList<String> param)
      {
         BB.out.println ("Hello from command");
         return 0;
      }

      @Override
      public String help()
      {
         return ("Shows the word 'Hello' on the terminal");
      }

      

      @Override
      public ArrayList<String> param_name()
      {
         return null;
      }
       
    }
  
    //----- new test command -----
    public static class CmdAdd implements ICommand
    {
       private final ArrayList<String> _param;
      public CmdAdd () 
      {
         _param = new ArrayList<String>();
         _param.add("Value 1");
         _param.add("Value 2");
         
      }
       
      @Override
      public int execute(ArrayList<String> param)
      {
         BB.out.println ("2 + 2 equals 4");
         return 0;
      }

      @Override
      public String help()
      {
         return ("Adds 2 number together passed in parameter\nValue 1: First value to add\nValue 2: Second value to add");
      }

      

      @Override
      public ArrayList<String> param_name()
      {
         return _param;
      }
       
    }
            
}

