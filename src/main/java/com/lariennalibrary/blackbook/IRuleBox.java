/*
 * Copyright 2019 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lariennalibrary.blackbook;

import java.util.Map;

/**
 * This is a game interface that will contains all the rules of the game. The
 * concept is the same as a LibGDX game class adaptor. The programmer implement
 * this interface and pass his class to the Black Book Launchers.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public interface IRuleBox extends ICommandLibrary
{  
   //TODO: RULEBOX convert rulebox as modules as they possibly only give commands, 
   //a description string, and some initialisation code to run
   //No need to create a new class for it. Command library will fusion as module.
   
   /*Company title */
   //public final String company;
   /*Title  of the game*/
   //public final String title;
   /*Version of the game*/
   //public final String version;
   /*Year of the game, including releases and updates*/
   //public final String year;
   /*Author or Game Designer */
   //public final String author; 
   
   /*public IRuleBox ( String company, String title, String version, String year, String author)
   {
      this.company = company;
      this.title = title;
      this.version = version;
      this.year = year;
      this.author = author;              
   }  */
   
   /**
    * Should return information about the game as a multi line string. It is 
    * recommended to print the following information: Name of the game, company
    * version number, Year of release, Name of the designer, etc.
    * 
    * @return Information about the game in a single multiline string.
    */
   public String gameinfo ();
        
   
   /**
    * This method is run during the initialisation of the application. There is
    * no save game currently loaded or created at this point. It's the idea place
    * to register commands for example.
    *     
    */
   public void application_startup ();
   //TODO RULEBOX DECOMPOSITION maybe decompose the stuff run in those methods 
   //for more flexibility and control by the framework
   
   
   /**
    * This method is run during the loading or creation of a new game. The player
    * will be in game afterwards. Could be useful to define to set the setup 
    * context during new games.
    * 
    * @param newgame indicated if the game is a new game or a previously loaded game    
    */
   public void game_start( boolean newgame );
   //TODO maybe those will be obsolete as it will be in commands supplied by the rulebox
   
   /**
    * This method will be run when the game, not the application, closes. It could 
    * be something that must be done when the game is over or after a game is 
    * closed.
    * 
    * @param gameover indicates that the game closed because it is finished.    
    */
   
   public void game_end( boolean gameover);
   

}
